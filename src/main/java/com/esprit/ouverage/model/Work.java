package com.esprit.ouverage.model;

import java.io.Serializable;

import java.util.Date;
import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;


import org.springframework.format.annotation.DateTimeFormat;

import com.sun.istack.NotNull;

@Entity
@Table(name = "work")
public class Work implements Serializable{
	
	@GeneratedValue(strategy = GenerationType.AUTO)
	@Id()
	private Long id;
	
	
	private String title;
	
	
	private String author;
	
	@DateTimeFormat(pattern = "yyyy-MM-dd")
	private Date edition;
	
	
	private Boolean rentable;
	
	
	private String category;
	
	
	private String publisher;
	
	private float price;
	private String language;
	
	private int quantity;

	@ManyToOne
	private Offre offre;
	
	public Offre getOffre() {
		return offre;
	}
	public boolean hasOffre() {
		
		return !(offre == null);
	}

	public void setOffre(Offre offre) {
		this.offre = offre;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public String getAuthor() {
		return author;
	}

	public void setAuthor(String author) {
		this.author = author;
	}

	public Date getEdition() {
		return edition;
	}

	public void setEdition(Date edition) {
		this.edition = edition;
	}

	public Boolean getRentable() {
		return rentable;
	}

	public void setRentable(Boolean rentable) {
		this.rentable = rentable;
	}
	public String getCategory() {
		return category;
	}
	public void setCategory(String category) {
		this.category = category;
	}
	public String getPublisher() {
		return publisher;
	}
	public void setPublisher(String publisher) {
		this.publisher = publisher;
	}
	public String getLanguage() {
		return language;
	}
	public void setLanguage(String language) {
		this.language = language;
	}

	
	public void deaffecterOffre() {
		this.offre = null;
	}

	
	public Work(Long id, String title, String author, Date edition, Boolean rentable, String category, String publisher,
			float price, String language, int quantity, Offre offre) {
		super();
		this.id = id;
		this.title = title;
		this.author = author;
		this.edition = edition;
		this.rentable = rentable;
		this.category = category;
		this.publisher = publisher;
		this.price = price;
		this.language = language;
		this.quantity = quantity;
		this.offre = offre;
	}
	public float getPrice() {
		return price;
	}
	public void setPrice(float price) {
		this.price = price;
	}
	public int getQuantity() {
		return quantity;
	}
	public void setQuantity(int quantity) {
		this.quantity = quantity;
	}
	public Work() {
		
	}
	
	
}
