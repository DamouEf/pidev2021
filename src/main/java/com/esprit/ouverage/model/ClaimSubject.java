package com.esprit.ouverage.model;

public enum ClaimSubject {
    Login_Errors,
    Section_Loading,
    Inappropriate_Content,
    Privacy_Issues,
    Profile_Configurations,
    Others
}
