package com.esprit.ouverage.model;

import java.io.Serializable;

import java.util.Date;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;


import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
@Entity
@Table(name = "operations")
public class Operation implements Serializable {

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private long id;
	private long idMembre;
	private Date operationDate;
	private float subTotal;
	private float tax;
	private float total;
	private OperationType operationType;
	private boolean validate;
	
	//@JsonBackReference (value = "ingredients")
    @OneToMany(mappedBy = "operation", cascade = CascadeType.REMOVE)
    private List<Item> items;
	public Operation() {
		super();
		// TODO Auto-generated constructor stub
	}
	public Operation(long id, long idMembre, Date operationDate, float subTotal, float tax, float total, OperationType operationType, boolean validate) {
		super();
		this.id = id;
		this.idMembre = idMembre;
		this.operationDate = operationDate;
		this.subTotal = subTotal;
		this.tax = tax;
		this.total = total;
		this.operationType = operationType;
		this.validate = validate;
	}
	public long getId() {
		return id;
	}
	public void setId(long id) {
		this.id = id;
	}
	public long getIdMembre() {
		return idMembre;
	}
	public void setIdMembre(long idMembre) {
		this.idMembre = idMembre;
	}
	public Date getOperationDate() {
		return operationDate;
	}
	public void setOperationDate(Date operationDate) {
		this.operationDate = operationDate;
	}
	public float getSubTotal() {
		return subTotal;
	}
	public void setSubTotal(float subTotal) {
		this.subTotal = subTotal;
	}
	public float getTax() {
		return tax;
	}
	public void setTax(float tax) {
		this.tax = tax;
	}
	public float getTotal() {
		return total;
	}
	public void setTotal(float total) {
		this.total = total;
	}
	
	public OperationType getOperationType() {
		return operationType;
	}
	public void setOperationType(OperationType operationType) {
		this.operationType = operationType;
	}
	public boolean getValidate() {
		return validate;
	}
	public void setValidate(boolean validate) {
		this.validate = validate;
	}
	
}
