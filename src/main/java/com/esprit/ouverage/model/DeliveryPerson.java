package com.esprit.ouverage.model;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;

@Entity
@Table(name ="DeliveryPerson")

public class DeliveryPerson implements Serializable {
	

	@Id
    @GeneratedValue(strategy = GenerationType.AUTO)
	
    private long id;
	private String name ;
	private String surname;
	private int rating ;
	private String phone;
	


    @OneToMany(mappedBy="person",cascade=CascadeType.ALL)
	
	private List<Delivery> delivery ;
	
	public DeliveryPerson() {
		super();
		// TODO Auto-generated constructor stub
	}
	
	public DeliveryPerson( String name, String surname, int rating, String phone) {
		super();
		this.name = name;
		this.surname = surname;
		this.rating = rating;
		this.phone = phone;
	}
	public long getId() {
		return id;
	}
	public void setId(long id) {
		this.id = id;
	}
	
	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public List<Delivery> getDelivery() {
		return delivery;
	}

	public void setDelivery(List<Delivery> delivery) {
		this.delivery = delivery;
	}

	public String getSurname() {
		return surname;
	}
	public void setSurname(String surname) {
		this.surname = surname;
	}
	public int getRating() {
		return rating;
	}
	public void setRating(int rating) {
		this.rating = rating;
	}
	public String getPhone() {
		return phone;
	}
	public void setPhone(String phone) {
		this.phone = phone;
	}	
}
