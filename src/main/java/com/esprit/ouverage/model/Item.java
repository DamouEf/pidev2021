package com.esprit.ouverage.model;

import com.fasterxml.jackson.annotation.JsonBackReference;


import javax.persistence.*;

import org.springframework.format.annotation.DateTimeFormat;

import java.io.Serializable;
import java.util.Date;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

@Entity
@Table(name = "Items")
public class Item  implements Serializable {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;
    @Temporal(TemporalType.DATE)
    @DateTimeFormat(pattern = "yyyy-MM-dd")
    private Date startDate;
	private long nbDays;
	private Date expireDate;
	private int quantity;
	private Float discount;
	private Float total;
    private boolean returned;
    private Date returnDate;


    @ManyToOne(optional=false)
    @JoinColumn(name = "work", referencedColumnName="id")
    private Work work;

       
    @ManyToOne(optional=false)
    @JoinColumn(name = "member", referencedColumnName="id")
    private User member;
    
    
    @JsonBackReference(value = "operations")
    @ManyToOne(optional=false)
    @JoinColumn(name = "operation",referencedColumnName="id")
    private Operation operation;



	public Item() {
		super();
		// TODO Auto-generated constructor stub
	}


	public Item(Long id, Date startDate, long nbDays, Date expireDate, int quantity, Float discount, Float total,
			boolean returned, Date returnDate, Work work, User member, Operation operation) {
		super();
		this.id = id;
		this.startDate = startDate;
		this.nbDays = nbDays;
		this.expireDate = expireDate;
		this.quantity = quantity;
		this.discount = discount;
		this.total = total;
		this.returned = returned;
		this.returnDate = returnDate;
		this.work = work;
		this.member = member;
		this.operation = operation;
	}


	public Long getId() {
		return id;
	}


	public void setId(Long id) {
		this.id = id;
	}


	public int getQuantity() {
		return quantity;
	}


	public void setQuantity(int quantity) {
		this.quantity = quantity;
	}


	public Float getTotal() {
		return total;
	}


	public void setTotal(Float total) {
		this.total = total;
	}


	public Work getWork() {
		return work;
	}


	public void setWork(Work work) {
		this.work = work;
	}


	public Operation getOperation() {
		return operation;
	}


	public void setOperation(Operation operation) {
		this.operation = operation;
	}





	public Date getStartDate() {
		return startDate;
	}





	public void setStartDate(Date startDate) {
		this.startDate = startDate;
	}





	public long getNbDays() {
		return nbDays;
	}





	public void setNbDays(long nbDays) {
		this.nbDays = nbDays;
	}


	public Date getExpireDate() {
		return expireDate;
	}


	public void setExpireDate(Date expireDate) {
		this.expireDate = expireDate;
	}


	public boolean isReturned() {
		return returned;
	}


	public void setReturned(boolean returned) {
		this.returned = returned;
	}


	public Date getReturnDate() {
		return returnDate;
	}


	public void setReturnDate(Date returnDate) {
		this.returnDate = returnDate;
	}


	public User getMember() {
		return member;
	}


	public void setMember(User member) {
		this.member = member;
	}


	public Float getDiscount() {
		return discount;
	}


	public void setDiscount(Float discount) {
		this.discount = discount;
	}




    	
}
