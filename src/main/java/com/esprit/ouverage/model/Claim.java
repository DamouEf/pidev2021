package com.esprit.ouverage.model;

//import javafx.scene.input.DataFormat;

import org.springframework.format.annotation.DateTimeFormat;

import javax.persistence.*;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import java.io.Serializable;
import java.util.Date;


@Entity
@Table(name = "claim")
public class Claim implements Serializable {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;

    @ManyToOne(fetch = FetchType.LAZY)
    private User userId;

    @Enumerated(EnumType.STRING)
    private ClaimSubject subject;

    private String content;

    @DateTimeFormat(pattern = "yyyy-MM-dd")
    private Date claimDate;

    @Enumerated(EnumType.STRING)
    private StatusClaim status;

    // Constructors

    public Claim() {
        super();
        this.claimDate = new Date();
        this.status = StatusClaim.PENDING;
    }

    public Claim(Long id
            , User userId
            , ClaimSubject subject
            , String content) {
        this.id = id;
        this.userId = userId;
        this.subject = subject;
        this.content = content;
        this.claimDate = new Date();
        this.status = StatusClaim.PENDING;
    }
    @Enumerated(EnumType.STRING)
    public StatusClaim getStatus() {
        return status;
    }

    @Enumerated(EnumType.STRING)
    public void setStatus(StatusClaim status) {
        this.status = status;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public User getUserId() {
        return userId;
    }

    public void setUserId(User userId) {
        this.userId = userId;
    }

    @Enumerated(EnumType.STRING)
    public ClaimSubject getSubject() {
        return subject;
    }

    @Enumerated(EnumType.STRING)
    public void setSubject(ClaimSubject subject) {
        this.subject = subject;
    }

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }

    public Date getClaimDate() {
        return claimDate;
    }

    public void setClaimDate(Date claimDate) {
        this.claimDate = claimDate;
    }


}
