package com.esprit.ouverage.model;

//import javafx.scene.input.DataFormat;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;
import org.springframework.format.annotation.DateTimeFormat;

import javax.persistence.*;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import java.io.Serializable;


@Entity
@Table(name = "Feedback")
public class Feedback implements Serializable{
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;

    @ManyToOne(fetch = FetchType.LAZY)
    private User userId;

    @ManyToOne(fetch = FetchType.LAZY)
    private Work workId;

    private Integer rating;
    private String message;


    // Constructors

    public Feedback() {
    }

    public Feedback(Long id
            , User userId
            , Integer rating
            , String message) {
        this.id = id;
        this.userId = userId;
        this.rating = rating;
        this.message = message;
    }

    public Feedback(Long id
            , User userId
            , Integer rating
            , String message
            , Work workId) {
        this.id = id;
        this.userId = userId;
        this.rating = rating;
        this.message = message;
        this.workId = workId;
    }


    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public User getUserId() {
        return userId;
    }

    public void setUserId(User userId) {
        this.userId = userId;
    }

    public Work getWorkId() {
        return workId;
    }

    public void setWorkId(Work workId) {
        this.workId = workId;
    }

    public Integer getRating() {
        return rating;
    }

    public void setRating(Integer rating) {
        this.rating = rating;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }
}
