package com.esprit.ouverage.model;

import java.io.Serializable;
import java.util.Date;
import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;


import org.springframework.format.annotation.DateTimeFormat;


@Entity
@Table(name = "offre")
public class Offre implements Serializable {
	
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Id()
	public Long id;
	
	@Column(unique = true)
	private  String name;
	
	
	private  float percentage;
	
	
	@DateTimeFormat(pattern = "yyyy-MM-dd")
	private Date startDate;
	
	@DateTimeFormat(pattern = "yyyy-MM-dd")
	private Date endDate;
	
	@OneToMany(cascade = CascadeType.ALL, mappedBy="offre")
	private Set<Work> books;
	
	
	public Long getId() {
		return id;
	}


	public void setId(Long id) {
		this.id = id;
	}


	



	public float getPercentage() {
		return percentage;
	}


	public void setPercentage(float percentage) {
		this.percentage = percentage;
	}


	public String getName() {
		return name;
	}


	public void setName(String name) {
		this.name = name;
	}


	public Date getStartDate() {
		return startDate;
	}


	public void setStartDate(Date startDate) {
		this.startDate = startDate;
	}


	public Date getEndDate() {
		return endDate;
	}


	public void setEndDate(Date endDate) {
		this.endDate = endDate;
	}

	
	

	public Offre(Long id, String name,float percentage,
			Date startDate, Date endDate, Set<Work> books) {
		super();
		this.id = id;
		this.name = name;
		this.percentage = percentage;
		this.startDate = startDate;
		this.endDate = endDate;
		this.books = books;
	}


	public Set<Work> getBooks() {
		return books;
	}


	public void setBooks(Set<Work> books) {
		this.books = books;
	}


	public Offre() {
	
	}
	
	
	
	
}
