package com.esprit.ouverage.model;

import javax.persistence.*;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;


@Entity
@Table(name = "wishlist")
public class WishList implements Serializable {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;


    @ManyToOne
//    @Column(name = "work_id")
    private Work idWork;

    private Long idUser;


    // Default Constructor
    public WishList() {
    }

    public WishList(Long id, Work idWork, Long idUser) {
        this.id = id;
        this.idWork = idWork;
        this.idUser = idUser;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Work getIdWork() {
        return idWork;
    }

    public void setIdWork(Work idWork) {
        this.idWork = idWork;
    }

    public Long getIdUser() {
        return idUser;
    }

    public void setIdUser(Long idUser) {
        this.idUser = idUser;
    }
}
