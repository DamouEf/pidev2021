package com.esprit.ouverage.model;

import java.io.Serializable;


import javax.persistence.*;


@Entity
@Table(name = "delivery")
public class Delivery implements Serializable{

	@Id
    @GeneratedValue(strategy = GenerationType.AUTO)
	
    private Long idDelivery;
	private String deliveryDate ;

	@Enumerated(EnumType.STRING)
	private ServiceType svc;
    private boolean delivered = false;
	


	@OneToOne(cascade = CascadeType.ALL)
    @JoinColumn(name = "ido", referencedColumnName = "id")
	private Operation operation;
	
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "idp" ,referencedColumnName = "id")
    private DeliveryPerson person;

	public Delivery() {
		super();
	this.person=new DeliveryPerson();
	this.operation=new Operation();
			// TODO Auto-generated constructor stub
	}
	
    public boolean isDelivered() {
		return delivered;
	}



	public void setDelivered(boolean delivered) {
		this.delivered = delivered;
	}


	public Long getIdDelivery() {
		return idDelivery;
	}
 
	public void setIdDelivery(Long idDelivery) {
		this.idDelivery = idDelivery;
	}




	public String getDeliveryDate() {
		return deliveryDate;
	}


	public void setDeliveryDate(String deliveryDate) {
		this.deliveryDate = deliveryDate;
	}

	@Enumerated(EnumType.STRING)
	public ServiceType getSvc() {
		return svc;
	}
	
	@Enumerated(EnumType.STRING)	
	public void setSvc(ServiceType svc) {
		this.svc = svc;
	}


	public Operation getOperation() {
		return operation;
	}


	public void setOperation(Operation operation) {
		this.operation = operation;
	}


	public DeliveryPerson getPerson() {
		return person;
	}


	public void setPerson(DeliveryPerson person) {
		this.person = person;
	}








}
