package com.esprit.ouverage.service;

import com.esprit.ouverage.model.Satisfaction;
import com.esprit.ouverage.model.User;
import com.esprit.ouverage.repository.SatisfactionRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

@Service
@Transactional
public class SatisfactionService implements ISatisfactionService {

    @Autowired
    SatisfactionRepository satisfactionAgent;


    @Override
    public List<Satisfaction> getAllSatisfaction() {
        return satisfactionAgent.findAll();
    }

    @Override
    public List<Satisfaction> getSatisfactionbyUser(Long user) {
        return satisfactionAgent.getSatisfactionByUserId(user);
    }

    @Override
    public Satisfaction getSatisfactionById(Long id) {
        return satisfactionAgent.findById(id).get();
    }

    @Override
    public void deleteSatisfaction(Long id) {
        satisfactionAgent.deleteById(id);
    }

    @Override
    public void saveAndUpdate(Satisfaction satisfaction) {
        satisfactionAgent.save(satisfaction);
    }
}
