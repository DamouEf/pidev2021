package com.esprit.ouverage.service;

import java.util.List;

import com.esprit.ouverage.model.Operation;
import com.esprit.ouverage.model.OperationType;



public interface OperationService {
	
	public List<Operation> getAllOperations();

	public Operation getOperationById(long id);

	public void saveOrUpdate(Operation operation);

	public void deleteOperation(long id);
	
	public List<Operation> findByOperationTypeAndIdMembreAndValidate(OperationType type,Long userid,boolean operationstatus);
    
	public Operation findCurentCartOperation(long userid);
	
    public int NBCurentCartOperation(long userid);

	public Operation findCurentRentOperation(long userid);
	
    public int NBCurentRentOperation(long userid);
    public List<Operation> findByIdMembre(long idmembre);
	//public List<Cart> findBySpecialite(String spec);
    
	
	//public List<Cart> Search(String spec, String pseudo);

}
