package com.esprit.ouverage.service;

import com.esprit.ouverage.model.Claim;
import com.esprit.ouverage.model.StatusClaim;
import com.esprit.ouverage.model.User;
import com.esprit.ouverage.repository.ClaimRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

@Service
@Transactional
public class ClaimService implements IClaimService{

    @Autowired
    ClaimRepository claimAgent;

    @Override
    public List<Claim> getAllClaims() {
        return claimAgent.findAll();
    }

    @Override
    public List<Claim> getClaimsbyUser(Long user) {
        return claimAgent.getClaimByUserId(user);
    }

    @Override
    public List<Claim> getClaimsbyUser_(StatusClaim status, Long user) {
        return claimAgent.getClaimByStatusAndUserId(status,user);
    }

    @Override
    public List<Claim> getClaimsbyStatus(StatusClaim status) {
        return claimAgent.getClaimByStatus(status);
    }

    @Override
    public List<Claim> getClaimsbyStatusAndUser(StatusClaim status, String pseudo) {
        return claimAgent.getClaimByStatusAndUserId(status,pseudo);
    }


    @Override
    public Claim getClaimById(Long id) {
        return claimAgent.findById(id).get();
    }

    @Override
    public void deleteClaim(Long id) {
        claimAgent.deleteById(id);
    }

    @Override
    public void saveAndUpdate(Claim claim) {
        claimAgent.save(claim);
    }


    public List<Claim> claimSearch(StatusClaim status, String pseudo){
        if (status == null && pseudo == null) return getAllClaims();
        if (status != null && pseudo == null ) return getClaimsbyStatus(status);
        if (status != null && pseudo != null) return getClaimsbyStatusAndUser(status,pseudo);

//        else if(status == null && user != null) return getClaimsbyUser(user);
        else return new ArrayList<Claim>();

    }

}
