package com.esprit.ouverage.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import com.esprit.ouverage.model.Operation;
import com.esprit.ouverage.model.OperationType;
import com.esprit.ouverage.repository.OperationRepository;



//pour dire que cette classe reprensete un service
@Service
//car je veut utiliser OperationRepository pour lancer des requettes sur la DB
//JE veut utiliser des transactions sur la DB
@Transactional
public class OperationServiceImpl implements OperationService{
	   //Automatic-Write-Read
		//utiliser le phenomene de l'injection de dependance pour optimiser l'espace memoire, eviter les objets permanants
		//c'est que il ya un objet doit etre cree/instancier automatiquement et doit etre injecter dans le code (objet agent)
		@Autowired 
		OperationRepository agent;

		@Override
		public List<Operation> getAllOperations() {
			// TODO Auto-generated method stub
			return (List<Operation>) agent.findAll();
		}

		@Override
		public Operation getOperationById(long id) {
			// TODO Auto-generated method stub
			return agent.findById(id).get();
		}

		@Override
		public void saveOrUpdate(Operation operation) {
			// TODO Auto-generated method stub
			agent.save(operation);
		}

		@Override
		public void deleteOperation(long id) {
			// TODO Auto-generated method stub
			agent.deleteById(id);
		}
		@Override
		public List<Operation> findByOperationTypeAndIdMembreAndValidate( OperationType type,Long userid,boolean operationstatus)
		{
			return agent.findByOperationTypeAndIdMembreAndValidate(type,userid,operationstatus);
		}
		
		@Override
		public Operation findCurentCartOperation (long userid)
		{
			return agent.findCurentCartOperation(userid);
		}
		@Override
		public int NBCurentCartOperation(long userid)
		{
			return agent.NBCurentCartOperation(userid);
		}
		
		@Override
		public Operation findCurentRentOperation (long userid)
		{
			return agent.findCurentRentOperation(userid);
		}
		@Override
		public int NBCurentRentOperation(long userid)
		{
			return agent.NBCurentRentOperation(userid);
		}
		
		@Override
		public List<Operation> findByIdMembre(long idmembre)
		{
			return agent.findByIdMembre(idmembre);
		}
		
		
		//@Override
		//public List<Cart> findBySpecialite(String spec) {
			// TODO Auto-generated method stub
			//return agent.findBySpecialite(spec);
		//}
		
		
		//@Override
		//public List<Cart> Search(String spec,String pseudo) {
		//	 TODO Auto-generated method stub
		//	return agent.Search(spec, pseudo);
		//}
	


}
