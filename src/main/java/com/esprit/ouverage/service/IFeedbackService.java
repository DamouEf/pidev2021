package com.esprit.ouverage.service;
import com.esprit.ouverage.model.Feedback;
import java.util.List;

public interface IFeedbackService {
    List<Feedback> getAllFeedback();
    List<Feedback> getFeedbackbyUser(Long user);
    Feedback getFeedbackById(Long id);
    void deleteFeedback(Long id);
    void saveAndUpdate(Feedback feedback);
}
