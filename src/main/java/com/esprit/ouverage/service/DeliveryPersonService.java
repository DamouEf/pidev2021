package com.esprit.ouverage.service;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.repository.query.Param;

import com.esprit.ouverage.model.DeliveryPerson;
import com.esprit.ouverage.model.User;
import com.esprit.ouverage.repository.DeliveryPersonRepository;

public interface DeliveryPersonService {
	
	
	public List<DeliveryPerson> getAllDeliveryPerson();
	 
	public DeliveryPerson getDeliveryPersonById(long id);
	  
	public void saveOrUpdate(DeliveryPerson deliveryPerson);
	 
	public void deleteDeliveryPerson(long id);
	 
//	public List<DeliveryPerson> findByRating(int  rating);

}
