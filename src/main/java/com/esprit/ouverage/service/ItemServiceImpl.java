package com.esprit.ouverage.service;

import java.sql.Array;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import com.esprit.ouverage.model.Item;
import com.esprit.ouverage.model.Operation;
import com.esprit.ouverage.model.User;
import com.esprit.ouverage.model.Work;
import com.esprit.ouverage.repository.ItemRepository;



//pour dire que cette classe reprensete un service
@Service
//car je veut utiliser OperationRepository pour lancer des requettes sur la DB
//JE veut utiliser des transactions sur la DB
@Transactional
public class ItemServiceImpl implements ItemService{
	   //Automatic-Write-Read
		//utiliser le phenomene de l'injection de dependance pour optimiser l'espace memoire, eviter les objets permanants
		//c'est que il ya un objet doit etre cree/instancier automatiquement et doit etre injecter dans le code (objet agent)
		@Autowired 
		ItemRepository agent;

		@Override
		public List<Item> getAllItems() {
			// TODO Auto-generated method stub
			return (List<Item>) agent.findAll();
		}

		@Override
		public Item getItemById(long id) {
			// TODO Auto-generated method stub
			return agent.findById(id).get();
		}

		@Override
		public void saveOrUpdate(Item item) {
			// TODO Auto-generated method stub
			agent.save(item);
		}

		@Override
		public void deleteItem(long id) {
			// TODO Auto-generated method stub
			agent.deleteById(id);
		}
		@Override
		public List<Item> findByOperation(Operation operation)
		{
			return agent.findByOperation(operation);
		}
		@Override
		public List<Item> findItemsOperation(Long operation)
		{
			return agent.findItemsOperation(operation);
		}
		
		@Override
		 public List<Item> findNonValidateItemCart(Long user)
		 {
			 return agent.findNonValidateItemCart(user);
		 }
		@Override
		 public Float SommeItemsCurentOperation(Operation operation)
		 {
			 return agent.SommeItemsCurentOperation(operation);
		 }
		@Override
		public long NBCurentItemWorkOperation(Operation operation,Work work)
		 {
			 return agent.NBCurentItemWorkOperation(operation,work);
		 }
		@Override
		 public Item findCurentItemWorkOperation(Operation operation,Work work)
		 {
			 return agent.findCurentItemWorkOperation(operation,work);
		 }
		@Override
		 public List<Item> findNonReturnedBook(User user)
		 {
			 return agent.findNonReturnedBook(user);
		 }
		 @Override
		 public List<Item> findByMember(User user)
		 {
			 return agent.findByMember(user);
		 }
		//@Override
		//public List<Cart> findBySpecialite(String spec) {
			// TODO Auto-generated method stub
			//return agent.findBySpecialite(spec);
		//}
		
		
		//@Override
		//public List<Cart> Search(String spec,String pseudo) {
		//	 TODO Auto-generated method stub
		//	return agent.Search(spec, pseudo);
		//}
	


}
