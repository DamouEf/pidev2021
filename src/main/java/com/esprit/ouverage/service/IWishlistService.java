package com.esprit.ouverage.service;

import com.esprit.ouverage.model.Claim;
import com.esprit.ouverage.model.User;
import com.esprit.ouverage.model.WishList;

import java.util.List;

public interface IWishlistService {

    List<WishList> getMyWishlist(Long userId);
    void deleteWishlist(Long id);
    void saveAndUpdate(WishList wishList);
}
