package com.esprit.ouverage.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.esprit.ouverage.model.Delivery;
import com.esprit.ouverage.model.DeliveryPerson;
import com.esprit.ouverage.repository.DeliveryRepository;


@Service
@Transactional
public class DeliveryServiceImpl implements DeliveryService{
	
	@Autowired
	DeliveryRepository agent;

	@Override
	public List<Delivery> getAllDelivery() {
		// TODO Auto-generated method stub
		return (List<Delivery>)agent.findAll();
	}

	@Override
	public Delivery getDeliveryById(long idDelivery) {
		// TODO Auto-generated method stub
		return agent.findById(idDelivery).get();
	}

	@Override
	public void saveOrUpdate(Delivery delivery) {
		// TODO Auto-generated method stub
		agent.save(delivery);
	}

	@Override
	public void deleteDelivery(long idDelivery) {
		// TODO Auto-generated method stub
		agent.deleteById(idDelivery);
	}

//	@Override
//	public List<Delivery> getUserDeliveries(long user) {
//		// TODO Auto-generated method stub
//		return agent.getUserDeliveries(user);
//	}
//	
	
	


}
