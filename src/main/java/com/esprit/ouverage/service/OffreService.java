package com.esprit.ouverage.service;

import java.util.List;

import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.esprit.ouverage.model.Offre;
import com.esprit.ouverage.repository.OffreRepository;

@Service
@Transactional
public class OffreService {
	@Autowired
	 OffreRepository Repo;
	

    public List<Offre> findAll(){
        return (List<Offre>) Repo.findAll();
    }
    
    public Offre findById(long id) {
    	Offre offre = Repo.findById(id).get();
        return offre;
    }


}
