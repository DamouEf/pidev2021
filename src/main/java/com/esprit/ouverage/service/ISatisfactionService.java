package com.esprit.ouverage.service;

import com.esprit.ouverage.model.Satisfaction;

import java.util.List;

public interface ISatisfactionService {

    List<Satisfaction> getAllSatisfaction();
    List<Satisfaction> getSatisfactionbyUser(Long user);
    Satisfaction getSatisfactionById(Long id);
    void deleteSatisfaction(Long id);
    void saveAndUpdate(Satisfaction satisfaction);

}
