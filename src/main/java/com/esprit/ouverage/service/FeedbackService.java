package com.esprit.ouverage.service;

import com.esprit.ouverage.model.Feedback;
import com.esprit.ouverage.model.Satisfaction;
import com.esprit.ouverage.repository.FeedbackRepository;
import com.esprit.ouverage.repository.SatisfactionRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.util.List;

@Service
@Transactional
public class FeedbackService implements IFeedbackService {

    @Autowired
    FeedbackRepository feedbackAgent;


    @Override
    public List<Feedback> getAllFeedback() {
        return feedbackAgent.findAll();
    }

    @Override
    public List<Feedback> getFeedbackbyUser(Long user) {
        return feedbackAgent.getFeedbackByUserId(user);
    }

    @Override
    public Feedback getFeedbackById(Long id) {
        return feedbackAgent.findById(id).get();
    }

    @Override
    public void deleteFeedback(Long id) {
        feedbackAgent.deleteById(id);
    }

    @Override
    public void saveAndUpdate(Feedback feedback) {
        feedbackAgent.save(feedback);
    }
}
