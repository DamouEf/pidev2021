package com.esprit.ouverage.service;

import com.esprit.ouverage.model.Claim;
import com.esprit.ouverage.model.StatusClaim;

import java.util.Date;
import java.util.List;

public interface IClaimService {

    List<Claim> getAllClaims();
    List<Claim> getClaimsbyUser(Long user);
    List<Claim> getClaimsbyUser_(StatusClaim status, Long user);
    List<Claim> getClaimsbyStatus(StatusClaim status);
    List<Claim> getClaimsbyStatusAndUser(StatusClaim status, String pseudo);
    Claim getClaimById(Long id);
    void deleteClaim(Long id);
    void saveAndUpdate(Claim claim);
}
