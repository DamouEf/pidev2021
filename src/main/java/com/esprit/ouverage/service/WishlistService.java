package com.esprit.ouverage.service;

import com.esprit.ouverage.model.User;
import com.esprit.ouverage.model.WishList;
import com.esprit.ouverage.repository.WishlistRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.util.List;

@Service
@Transactional
public class WishlistService implements IWishlistService {


    @Autowired
    WishlistRepository wishlistAgent;

    @Override
    public List<WishList> getMyWishlist(Long userId) {
        return wishlistAgent.findAllByIdUserOrOrderByCreatedDate(userId);
    }

    @Override
    public void deleteWishlist(Long id) {
        wishlistAgent.deleteById(id);
    }

    @Override
    public void saveAndUpdate(WishList wishList) {
        wishlistAgent.save(wishList);
    }
}
