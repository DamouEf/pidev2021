package com.esprit.ouverage.service;

import java.util.List;

import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;


import com.esprit.ouverage.model.Delivery;
import com.esprit.ouverage.model.Item;



public interface DeliveryService {

	
	public List<Delivery> getAllDelivery();
	
	 
	 public Delivery getDeliveryById(long idDelivery);
	  
	 public void saveOrUpdate(Delivery delivery);
	 
	 public void deleteDelivery(long idDelivery);
	 
	// public List<Delivery> getUserDeliveries(long user);
	 
	 
}

