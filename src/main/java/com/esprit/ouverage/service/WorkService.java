package com.esprit.ouverage.service;

import java.util.List;

import com.esprit.ouverage.model.Work;
import java.util.Set;

import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.esprit.ouverage.model.Offre;
import com.esprit.ouverage.model.Work;
import com.esprit.ouverage.repository.OffreRepository;
import com.esprit.ouverage.repository.WorkRepository;


@Service
@Transactional
public class WorkService {
	
	
	@Autowired
	WorkRepository workRepo;
	@Autowired
	OffreRepository offreRepo;
	 
	public void save(Work work) {
		workRepo.save(work);
	}

	public Work findById(long id) {
		Work user = workRepo.findById(id).get();
		return user;
	}
	
	public List<Work> findAll(){
		return (List<Work>) workRepo.findAll();
	}

	public void Delete (long id) {
		workRepo.deleteById(id);
	}
	    
	public void desaffecterOffre(long idBook,long idOffre) {
		Work book = workRepo.findById(idBook)
					.orElseThrow(() -> new IllegalArgumentException("Invalid book Id:" + idBook));
		Offre offre = offreRepo.findById(idOffre) .orElseThrow(() -> new IllegalArgumentException("Invalid offre Id:" ));
		Set<Work> listBooks = 	offre.getBooks();
		listBooks.remove(book);
		offre.setBooks(listBooks);
		offreRepo.save(offre);
		book.deaffecterOffre();
		workRepo.save(book);
	}

	public void affecterOffre(long idBook, long idOffre) {
		Work book = workRepo.findById(idBook)
					.orElseThrow(() -> new IllegalArgumentException("Invalid book Id:" + idBook));
		Offre offre = offreRepo.findById(idOffre) .orElseThrow(() -> new IllegalArgumentException("Invalid offre Id:" ));
		book.setOffre(offre);
		workRepo.save(book);
		Set<Work> listBooks = 	offre.getBooks();
		listBooks.add(book);
		offre.setBooks(listBooks);
		offreRepo.save(offre);
	}

 	// TODO check names of function to adapt with sonia's work 

//	public List<Work> getAllWorks();
//
//	public Work getWorkById(long id);
//
//	public void saveOrUpdate(Work work);
//
//	public void deleteWork(long id);
//	
//	public List<Work> getByTitre(String titre);
//	public List<Work> getByAuthor(String Author);
//    public List<Work> getByTitreAndAuthor(String titre,String author);
//    public List<Work> workSearcher(String titre, String author);
//    public List<Work> workSearcher1(String titre, String author);
}
