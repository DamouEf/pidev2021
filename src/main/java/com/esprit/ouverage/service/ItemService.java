package com.esprit.ouverage.service;

import java.sql.Array;
import java.util.List;

import org.springframework.data.repository.query.Param;

import com.esprit.ouverage.model.Item;
import com.esprit.ouverage.model.Operation;
import com.esprit.ouverage.model.User;
import com.esprit.ouverage.model.Work;




public interface ItemService {
	
	public List<Item> getAllItems();

	public Item getItemById(long id);

	public void saveOrUpdate(Item item);

	public void deleteItem(long id);
	
	public List<Item> findByOperation(Operation operation);

	public List<Item> findItemsOperation( Long operation);
	
	 public List<Item> findNonValidateItemCart(Long user);
	
	 public Float SommeItemsCurentOperation(Operation operation);
	 
	 public long NBCurentItemWorkOperation(Operation operation,Work work);
	 
	 public Item findCurentItemWorkOperation(Operation operation,Work work);
	 
	 public List<Item> findNonReturnedBook(User user); 
	 
	 public List<Item> findByMember(User user);
	 //public List<Cart> findBySpecialite(String spec);
    
	
	//public List<Cart> Search(String spec, String pseudo);

}
