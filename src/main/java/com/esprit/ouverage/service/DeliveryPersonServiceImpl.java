package com.esprit.ouverage.service;

import java.util.List;



import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.esprit.ouverage.model.DeliveryPerson;
import com.esprit.ouverage.repository.DeliveryPersonRepository;

@Service
@Transactional

public class DeliveryPersonServiceImpl implements DeliveryPersonService{
	@Autowired
	DeliveryPersonRepository agent;

	@Override
	public List<DeliveryPerson> getAllDeliveryPerson() {
		// TODO Auto-generated method stub
		return (List<DeliveryPerson>)agent.findAll();
	}

	@Override
	public DeliveryPerson getDeliveryPersonById(long id) {
		// TODO Auto-generated method stub
		return agent.findById(id).get();
	}

	@Override
	public void saveOrUpdate(DeliveryPerson deliveryPerson) {
		// TODO Auto-generated method stub
		agent.save(deliveryPerson);
	}

	@Override
	public void deleteDeliveryPerson(long id) {
		// TODO Auto-generated method stub
		agent.deleteById(id);
	}

	/*
	 * @Override public List<DeliveryPerson> findByRating(Integer rating) { // TODO
	 * Auto-generated method stub return agent.findByRating(rating); }
	 */
}
