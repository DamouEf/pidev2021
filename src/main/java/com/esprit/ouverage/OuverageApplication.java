package com.esprit.ouverage;

import java.text.SimpleDateFormat;
import java.util.Date;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class OuverageApplication {

	public static void main(String[] args) {
		SpringApplication.run(OuverageApplication.class, args);
	}

}
