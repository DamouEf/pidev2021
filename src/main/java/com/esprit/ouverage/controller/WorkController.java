package com.esprit.ouverage.controller;

import java.util.ArrayList;

import java.util.List;
import java.util.Set;


import com.esprit.ouverage.config.CurrentUserFinder;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;

import com.esprit.ouverage.config.CurrentUserFinder;
import com.esprit.ouverage.model.Offre;
import com.esprit.ouverage.model.User;
import com.esprit.ouverage.model.Work;
import com.esprit.ouverage.repository.OffreRepository;
import com.esprit.ouverage.repository.WorkRepository;
import com.esprit.ouverage.service.WorkService;

@Controller
@RequestMapping(path = "/book")
public class WorkController {

	@Autowired
	WorkService workService;
	
	@Autowired
	WorkRepository workRepo;
	@Autowired
	OffreRepository offreRepo;
	@Autowired
	CurrentUserFinder currentUserFinder;
	 
	@GetMapping
	@PreAuthorize("hasRole('ROLE_USER')")
	public String userHome(Model model) {
		List<Work> workList = workService.findAll();
		User currentUser = currentUserFinder.getCurrentUser();
		model.addAttribute("user", currentUser);
		model.addAttribute("workList", workList);
		return "Member/Show-work";
	}
	   	
	@GetMapping(value = "list")
	@PreAuthorize("hasRole('ROLE_LIBRARIAN')")
	public String libririanTable(Model model) {
		List<Work> workList = workService.findAll();
		model.addAttribute("workList", workList);
		return "Librarian/list-book";
	}

	@PreAuthorize("hasRole('ROLE_LIBRARIAN')")
	@RequestMapping(value="/add-save", method=RequestMethod.POST)
	public String save(@ModelAttribute(value ="newWork")  Work newWork, BindingResult result, Model model) {
		if (result.hasErrors()) {
			return "Librarian/add-work";
		}
		workRepo.save(newWork);
	return "redirect:/book/list";
	}

	@PreAuthorize("hasRole('ROLE_LIBRARIAN')")
	@RequestMapping(value="/add", method=RequestMethod.GET)
	public String addBookForm(Model model) {
	model.addAttribute("newWork",new Work());
	return "Librarian/add-work";
	}
		
		 
	@GetMapping("/edit/{id}")
	@PreAuthorize("hasRole('ROLE_LIBRARIAN')")
	public String showUpdateForm(@PathVariable("id") long id, Model model) {
		Work work = workRepo.findById(id).orElseThrow(() -> new IllegalArgumentException("Invalid book Id:" + id));
		model.addAttribute("book", work);
		
		return "Librarian/update-book";
	}

	@PostMapping("/edit/{id}")
	@PreAuthorize("hasRole('ROLE_LIBRARIAN')")
	public String updateBook(@PathVariable("id") long id, Work book, BindingResult result, Model model) {
		if (result.hasErrors()) {
			book.setId(id);
			return "Librarian/update-book";
		}
		Work  book1 = workRepo.findById(id).orElseThrow(() -> new IllegalArgumentException("Invalid book Id:" + id));
		
		book.setOffre(book1.getOffre());
		workRepo.save(book);
		return "redirect:/book/list";
	}

	@GetMapping("/delete/{id}")
	@PreAuthorize("hasRole('ROLE_LIBRARIAN')")
	public String deleteBook(@PathVariable("id") long id, Model model) {
		Work book = workRepo.findById(id)
			.orElseThrow(() -> new IllegalArgumentException("Invalid book Id:" + id));
		workRepo.delete(book);
		return "redirect:/book/list";
	}
		 	
	@GetMapping("/affecter-offre/{id}")
	@PreAuthorize("hasRole('ROLE_LIBRARIAN')")
	public String affecterBook(@PathVariable("id") long id, Model model) {
		Work book = workRepo.findById(id)
			.orElseThrow(() -> new IllegalArgumentException("Invalid book Id:" + id));
		model.addAttribute("book", book);
		model.addAttribute("offres", offreRepo.findAll());
		return "Librarian/affecter-offre";
	}

	@PostMapping("/affecter-offre/{id}")
	@PreAuthorize("hasRole('ROLE_LIBRARIAN')")
	public String affecterBookTraitement(@PathVariable("id") long id,@RequestParam("offre") String  offreId, Model model) {
	workService.affecterOffre(id,(long) Integer.parseInt(offreId));
		return "redirect:/book/list";
	}

	@GetMapping("/desaffecter-offre/{id}/{idOffre}")
	@PreAuthorize("hasRole('ROLE_LIBRARIAN')")
	public String désaffecterBookTraitement(@PathVariable("id") long id,@PathVariable("idOffre") long idOffre, Model model) {
			workService.desaffecterOffre(id, idOffre);
		return "redirect:/book/list";
	}
		 	
	@GetMapping("/search")
	public String searchBooks(	@RequestParam(value = "authot_title", required = true) String authorTitle,
								@RequestParam(value = "disponible_rent", required = true) boolean rent,
								@RequestParam(value = "offre", required = true) boolean offre,
								Model model) {
		ArrayList<Work> books ; 
		if(authorTitle.isEmpty()) {
			books =  (ArrayList<Work>) workRepo.findAll();
		}else {
			books =  workRepo.searchBooks(authorTitle);
		}
		if(rent && offre) {
			
			model.addAttribute("workList",books.stream().filter(x -> x.getRentable() == true)
					.filter(x -> x.hasOffre()).toArray(Work[]::new));
		}else if(!rent && offre) {
			model.addAttribute("workList",books.stream().filter(x ->  x.getRentable() == false)
					.filter(x -> x.hasOffre()).toArray(Work[]::new));
		}
		else if(rent && !offre) {
			model.addAttribute("workList",books.stream().filter(x -> !x.hasOffre())
					.filter(x -> x.getRentable() == true).toArray(Work[]::new));
		}else {
			model.addAttribute("workList",books.stream().filter(x ->  x.getRentable() == false)
					.filter(x -> !x.hasOffre()).toArray(Work[]::new));
		}

		return "Member/Show-work";
	}
}
