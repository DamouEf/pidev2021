package com.esprit.ouverage.controller;

import com.esprit.ouverage.config.CurrentUserFinder;
import com.esprit.ouverage.model.*;
import com.esprit.ouverage.service.FeedbackService;
import com.esprit.ouverage.service.SatisfactionService;
import com.esprit.ouverage.service.UserService;
import com.esprit.ouverage.service.WorkService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.annotation.Secured;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.stream.Collectors;

@Controller
@RequestMapping("/feedbacks")
public class FeedbackController {

    @Autowired
    FeedbackService feedbackService;
    @Autowired
    WorkService workService;
    @Autowired
    UserService usService;
    @Autowired
    CurrentUserFinder currentUserFinder;

    @GetMapping(value = "/managefeedbacks")
    @PreAuthorize("hasRole('ROLE_LIBRARIAN') or hasRole('ROLE_USER')")
    public String manageFeedback(
            Model model) {

        //Menu Selection
        String menu="userNavBar";
        User currentUser = currentUserFinder.getCurrentUser();
        if(currentUser.getRole().equals("ROLE_USER")){
            menu = "userNavBar";
        }else { menu = "employeeNavBar"; }

        List<Feedback> feedbacks = feedbackService.getAllFeedback();
        model.addAttribute("user",currentUser);
        model.addAttribute("NavBar",menu); // select menu
        model.addAttribute("feedbacks", feedbacks);
        return "Feedbacks/manage-feedbacks";
    }


    @GetMapping(value = "/feedbacksbywork")
    @PreAuthorize("hasRole('ROLE_LIBRARIAN') or hasRole('ROLE_USER')")
    public String manageFeedbackByWork(
            @RequestParam Long workId,
            Model model) {

        //Menu Selection
        String menu="userNavBar";
        User currentUser = currentUserFinder.getCurrentUser();
        if(currentUser.getRole().equals("ROLE_USER")){
            menu = "userNavBar";
        }else { menu = "employeeNavBar"; }
        List<Feedback> feedbacks = feedbackService.getAllFeedback().stream().filter(feedback -> feedback.getWorkId().getId().equals(workId)).collect(Collectors.toList());
        model.addAttribute("user",currentUser);
        model.addAttribute("NavBar",menu); // select menu
        model.addAttribute("feedbacks", feedbacks);
        return "Feedbacks/manage-feedbacks";
    }


    // ADD feedback (USER)
    @GetMapping("/addfeedback")
    @PreAuthorize("hasRole('ROLE_LIBRARIAN') or hasRole('ROLE_USER')")
    public String addFeedbakc (@RequestParam Long workId, Model model) {
        Work work = workService.findById(workId);
        Feedback newFeedback = new Feedback();
        newFeedback.setWorkId(work);
        model.addAttribute("newfeedback", newFeedback);
        return "Feedbacks/add-feedback";
    }

    // SAVE/Persist Feedback (USER)
    @PostMapping("/savefeedback/{workId}")
    @PreAuthorize("hasRole('ROLE_LIBRARIAN') or hasRole('ROLE_USER')")
    public String SaveFeedbacks(@PathVariable("workId") long workId ,Feedback newFeedback) throws Exception {
        //throw new Exception(String.valueOf(workId));
        Work work = workService.findById(workId);
        newFeedback.setUserId(currentUserFinder.getCurrentUser());
        newFeedback.setWorkId(work);
        feedbackService.saveAndUpdate(newFeedback);
        return "redirect:/feedbacks/managefeedbacks";
    }

    // MODIFY Feedback Infos
    @GetMapping(value="/managefeedback")
    @PreAuthorize("hasRole('ROLE_LIBRARIAN') or hasRole('ROLE_USER')")
    public String manageFeedback(@RequestParam Long feedbackId,
                                     Model model) {
        User currentUser = currentUserFinder.getCurrentUser();
        Feedback feedback = feedbackService.getFeedbackById(feedbackId);
        model.addAttribute("feedback", feedback);
        model.addAttribute("currentUser", currentUser);
        return "Feedbacks/manage-feedback";
    }


    // UPDATE Feedback Infos USER
    @PostMapping(value="/feedbackinfomodify/{workId}")
    @PreAuthorize("hasRole('ROLE_LIBRARIAN') or hasRole('ROLE_USER')")
    public String updatedFeedbackInfos(     @PathVariable("workId") long workId,
                                            @RequestParam String feedbackNewMessage,
                                            //@RequestParam Integer feedbackNewRating,
                                            @RequestParam Long feedbackId) {
        Feedback feedback = feedbackService.getFeedbackById(feedbackId);
        feedback.setMessage(feedbackNewMessage);
        //feedback.setRating(feedbackNewRating);
        Work work = workService.findById(workId);
        feedback.setWorkId(work);
        feedbackService.saveAndUpdate(feedback);
        return "redirect:/feedbacks/managefeedbacks";
    }


    // DELETE Feedback (User)
    @GetMapping("/deletefeedback")
    @PreAuthorize("hasRole('ROLE_USER') or hasRole('ROLE_LIBRARIAN')")
    public String deleteFeedback(@RequestParam Long feedbackId){
        feedbackService.deleteFeedback(feedbackId);
        return "redirect:/feedbacks/managefeedbacks";
    }



}
