package com.esprit.ouverage.controller;

import com.esprit.ouverage.config.CurrentUserFinder;
import com.esprit.ouverage.model.User;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;

@Controller
@RequestMapping(path = "/librarian")

public class LibrarianController {

    @Autowired
    CurrentUserFinder currentUserFinder;
    @GetMapping
    @PreAuthorize("hasRole('ROLE_LIBRARIAN')")
    public String librarianHome(Model model) {
        User currentUser = currentUserFinder.getCurrentUser();
//        model.addAttribute("booksWithFines", fineCalculator.selectBooksWithFines(currentUser.getBooks()));
        model.addAttribute("currentUser", currentUser);
        return "Librarian/librarian-home";
    }

}
