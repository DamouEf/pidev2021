package com.esprit.ouverage.controller;

import com.esprit.ouverage.config.CurrentUserFinder;
import com.esprit.ouverage.model.Delivery;
import com.esprit.ouverage.model.DeliveryPerson;
import com.esprit.ouverage.model.Item;
import com.esprit.ouverage.model.Operation;
import com.esprit.ouverage.model.OperationType;
import com.esprit.ouverage.model.Satisfaction;
import com.esprit.ouverage.model.ServiceType;
import com.esprit.ouverage.model.StatusClaim;
import com.esprit.ouverage.model.User;
import com.esprit.ouverage.model.Work;
import com.esprit.ouverage.service.DeliveryPersonService;
import com.esprit.ouverage.service.DeliveryService;
import com.esprit.ouverage.service.ItemService;
import com.esprit.ouverage.service.OperationService;
import com.esprit.ouverage.service.UserService;
import com.esprit.ouverage.service.WorkService;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.annotation.Secured;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@Controller

@RequestMapping("/operations")
public class OperationController {

    @Autowired
    DeliveryService deliveryService;
    @Autowired
    DeliveryPersonService deliverypersonService;
    @Autowired
    OperationService operationService;
    @Autowired
    ItemService itemService;
    @Autowired
    WorkService workService;
    @Autowired
    UserService usService;
    @Autowired
    CurrentUserFinder currentUserFinder;

    @GetMapping(value = "/manageoperations1")
    @PreAuthorize("hasRole('ROLE_USER')")
    public String manageOperations1(@RequestParam (required = false) boolean operationStatus1 ,
    		@RequestParam (required = false) OperationType operationType1,
            Model model) {

        //Menu Selection
        String menu="userNavBar";
        User currentUser = currentUserFinder.getCurrentUser();
        if(currentUser.getRole().equals("ROLE_USER")){
            menu = "userNavBar";
        }else { menu = "employeeNavBar"; }
        List<Operation> carts = operationService.findByOperationTypeAndIdMembreAndValidate(operationType1,currentUser.getId(),operationStatus1);

        
        model.addAttribute("carts", carts);
        model.addAttribute("user",currentUser);
        model.addAttribute("NavBar",menu); // select menu
        
        return "operations/manage-operations1";
    }
    
    
    
    @GetMapping(value = "/manageoperations")
    @PreAuthorize("hasRole('ROLE_USER')")
    public String manageOperations(@RequestParam (required = false) boolean operationStatus1 ,
    		@RequestParam (required = false) boolean operationStatus2,
    		@RequestParam (required = false) OperationType operationType1,
    		@RequestParam (required = false) OperationType operationType2,
            Model model) {

        //Menu Selection
        String menu="userNavBar";
        User currentUser = currentUserFinder.getCurrentUser();
        if(currentUser.getRole().equals("ROLE_USER")){
            menu = "userNavBar";
        }else { menu = "employeeNavBar"; }
        List<Operation> carts = operationService.findByOperationTypeAndIdMembreAndValidate(OperationType.CART,currentUser.getId(),operationStatus1);
        List<Operation> rents = operationService.findByOperationTypeAndIdMembreAndValidate(OperationType.RENT,currentUser.getId(),operationStatus2);
        
        model.addAttribute("rents", rents);
        model.addAttribute("carts", carts);
        model.addAttribute("user",currentUser);
        model.addAttribute("NavBar",menu); // select menu
        
        return "operations/manage-operations";
    }
    @GetMapping(value = "/manageoperations/CartSearch")
    @PreAuthorize("hasRole('ROLE_USER')")
    public String CartSearch(@RequestParam (required = false) boolean operationStatus,
    		@RequestParam (required = false) OperationType operationType,
            Model model) {

        //Menu Selection
        String menu="userNavBar";
        User currentUser = currentUserFinder.getCurrentUser();
        if(currentUser.getRole().equals("ROLE_USER")){
            menu = "userNavBar";
        }else { menu = "employeeNavBar"; }
        List<Operation> carts = operationService.findByOperationTypeAndIdMembreAndValidate(OperationType.CART,currentUser.getId(),operationStatus);
        model.addAttribute("carts", carts);
        model.addAttribute("user",currentUser);
        model.addAttribute("NavBar",menu); // select menu
        
        return "operations/manage-operations1";
    }
    
    @GetMapping(value = "/manageoperations/RentSearch")
    @PreAuthorize("hasRole('ROLE_USER')")
    public String RentSearch(@RequestParam (required = false) boolean operationStatus,
    		@RequestParam (required = false) OperationType operationType,
            Model model) {

        //Menu Selection
        String menu="userNavBar";
        User currentUser = currentUserFinder.getCurrentUser();
        if(currentUser.getRole().equals("ROLE_USER")){
            menu = "userNavBar";
        }else { menu = "employeeNavBar"; }
        List<Operation> rents = operationService.findByOperationTypeAndIdMembreAndValidate(OperationType.RENT,currentUser.getId(),operationStatus);
        model.addAttribute("rents", rents);
        model.addAttribute("user",currentUser);
        model.addAttribute("NavBar",menu); // select menu
        
        return "operations/manage-operations1";
    }
   
    @GetMapping("/deleteoperation")
    @PreAuthorize("hasRole('ROLE_USER') or hasRole('ROLE_LIBRARIAN')")
    public String deleteOperation(@RequestParam Long Id){
    	//Menu Selection
        String menu="userNavBar";
        User currentUser = currentUserFinder.getCurrentUser();
        if(currentUser.getRole().equals("ROLE_USER")){
            menu = "userNavBar";
        }else { menu = "employeeNavBar"; }
    	//update qte works 
        Operation operation=operationService.getOperationById(Id);
        List<Item> items = itemService.findByOperation(operation);
        for (Item item : items) {
        	//update qte work
            Work work=item.getWork();
            work.setQuantity(work.getQuantity()+item.getQuantity());
            workService.save(work);	
        }
        operationService.deleteOperation(Id);
        return "redirect:/operations/manageoperations1";
    }
//    @GetMapping(value="/deleteoperation/areyousuretodeleteoperation")
//	public String areYouSureToDeleteOperation(@RequestParam Long Id, Model model) {
//		Operation operation = operationService.getOperationById(Id);
//		model.addAttribute("operation", operation);
//		return "operations/operation-delete-operation.html";
//	}
    
    @GetMapping(value = "/addoperation")
    @PreAuthorize("hasRole('ROLE_USER')")
    public String addoperation(
            Model model) {

    	String menu="userNavBar";
        User currentUser = currentUserFinder.getCurrentUser();
        if(currentUser.getRole().equals("ROLE_USER")){
            menu = "userNavBar";
        }else { menu = "employeeNavBar"; }

        return "redirect:/book";    
        }
//    @GetMapping(value="/reservations")
//	public String reservations(Model model) {
//		model.addAttribute("unprocessedReservations", bookService.getUnprocessedBookReservations());
//		model.addAttribute("processedReservations", bookService.getProcessedBookReservations());
//		return "employee/employee-reservations.html";
//	}
	
	@GetMapping(value="/operationValidate")
	public String operationValidate(@RequestParam Long operationId, 
									@RequestParam Long userId,
									Model model) {

    	String menu="userNavBar";
        User currentUser = currentUserFinder.getCurrentUser();
        if(currentUser.getRole().equals("ROLE_USER")){
            menu = "userNavBar";
        }else { menu = "employeeNavBar"; }
		
        model.addAttribute("user", usService.findById(userId));
		model.addAttribute("operation",operationService.getOperationById(operationId));
		return "operations/operation-validate.html";
	}
	
	@PostMapping(value="/operationValidate/isValidated")
    @PreAuthorize("hasRole('ROLE_USER')")
    public String isValidated( @RequestParam Long operationId) {
		String menu="userNavBar";
        User currentUser = currentUserFinder.getCurrentUser();
        if(currentUser.getRole().equals("ROLE_USER")){
            menu = "userNavBar";
        }else { menu = "employeeNavBar"; }
		Operation operation=operationService.getOperationById(operationId);
		operation.setValidate(true);
		operationService.saveOrUpdate(operation);
		Delivery delivery=new Delivery();
		if(operation.getOperationType()==OperationType.CART)
		{delivery.setOperation(operation);
		delivery.setSvc(ServiceType.SaleService);
		DeliveryPerson d=deliverypersonService.getAllDeliveryPerson().stream().findFirst().get();
		delivery.setPerson(d);}
		if(operation.getOperationType()==OperationType.RENT)
		{delivery.setOperation(operation);
		delivery.setSvc(ServiceType.RentService);
		DeliveryPerson d=deliverypersonService.getAllDeliveryPerson().stream().findFirst().get();
		delivery.setPerson(d);}
		deliveryService.saveOrUpdate(delivery);
		return "operations/operation-isvalidated.html";
	}
}
