package com.esprit.ouverage.controller;
import java.util.List;

import com.esprit.ouverage.config.CurrentUserFinder;
import com.esprit.ouverage.model.User;
import com.esprit.ouverage.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;


@Controller
@RequestMapping(value="/admin")
@PreAuthorize("hasRole('ROLE_ADMIN')")
public class AdminController {

    @Autowired
    UserService usService;

    @Autowired
    CurrentUserFinder currentUserFinder;

    @GetMapping
    public String adminHome(Model model) {
        User currentUser = currentUserFinder.getCurrentUser();
        model.addAttribute("currentUser", currentUser);
        return "Admin/admin-home";
    }


    @GetMapping(value="/manageaccounts")
    public String manageAuthorities(@RequestParam (required = false) String firstName,
                                    @RequestParam (required = false) String lastName,
                                    Model model) {
        List<User> users = usService.userSearcher(firstName, lastName);

        model.addAttribute("users", users);
        model.addAttribute("firstName", firstName);
        model.addAttribute("lastName", lastName);
        return "Admin/admin-manage-accounts";
    }

    @GetMapping(value="/manageaccount")
    public String manageAccount(@RequestParam Long userId,
                                Model model) {

        User user = usService.findById(userId);
        model.addAttribute("user", user);
        return "Admin/admin-manage-account";
    }

    @PostMapping(value="/confirmaccountsettings")
    public String confirmAccountChanges(@RequestParam boolean accStatus,
                                        @RequestParam String role,
                                        @RequestParam Long userId,
                                        Model model) {
        model.addAttribute("role", role);
        model.addAttribute("accStatus", accStatus);
        model.addAttribute("user", usService.findById(userId));
        return "Admin/admin-confirm-account-settings";
    }

    @PostMapping(value="/saveaccountsettings")
    public String saveAccountSettings(@RequestParam boolean accStatus,
                                      @RequestParam String role,
                                      @RequestParam Long userId) {
        User user = usService.findById(userId);
        user.setRole(role);
        user.setEnabled(accStatus);
        usService.save(user);
        return "redirect:/admin/accountsettingssaved";
    }

    @GetMapping(value="/accountsettingssaved")
    public String accountSettingsSaved() {
        return "Admin/admin-account-settings-saved";
    }

}