package com.esprit.ouverage.controller;


import com.esprit.ouverage.config.CurrentUserFinder;
import com.esprit.ouverage.model.Item;
import com.esprit.ouverage.model.Offre;
import com.esprit.ouverage.model.Operation;
import com.esprit.ouverage.model.OperationType;
import com.esprit.ouverage.model.Satisfaction;
import com.esprit.ouverage.model.User;
import com.esprit.ouverage.model.Work;
import com.esprit.ouverage.repository.ItemRepository;
import com.esprit.ouverage.repository.OperationRepository;
import com.esprit.ouverage.repository.WorkRepository;
import com.esprit.ouverage.service.ItemService;
import com.esprit.ouverage.service.OperationService;
import com.esprit.ouverage.service.UserService;
import com.esprit.ouverage.service.WorkService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.format.annotation.DateTimeFormat;
import org.springframework.lang.Nullable;
import org.springframework.security.access.annotation.Secured;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.ModelAndView;

import java.sql.Array;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

@Controller
@RequestMapping("/operations")
public class ItemController {

    @Autowired
    OperationService operationService;
    @Autowired
    ItemService itemService;
    @Autowired
    UserService usService;
    @Autowired
    CurrentUserFinder currentUserFinder;
    @Autowired
    WorkService workService;
    @Autowired
	OperationRepository agent;
    ItemRepository agent1;
    WorkRepository agent3;


    @GetMapping(value = "/manageoperations/managecartitems/{operation}")
    @PreAuthorize("hasRole('ROLE_USER')")
    public String managecartItems(@PathVariable Long operation,
            Model model) {

        //Menu Selection
        String menu="userNavBar";
        User currentUser = currentUserFinder.getCurrentUser();
        float total;
        OperationType operationtype;
        boolean operationValidate;
        if(currentUser.getRole().equals("ROLE_USER")){
            menu = "userNavBar";
        }else { menu = "employeeNavBar"; }
        Operation op = operationService.getOperationById(operation); 
         total=op.getTotal();
         operationtype=op.getOperationType();
         operationValidate=op.getValidate();
         List<Item> items = itemService.findByOperation(op);
        model.addAttribute("user",currentUser);
        model.addAttribute("NavBar",menu); // select menu
        model.addAttribute("items", items);
        model.addAttribute("total", total);
        model.addAttribute("operationtype",operationtype );
        model.addAttribute("operationValidate",operationValidate );
        model.addAttribute("operation",operation );
        return "operations/items/manage-cartitems";
    }
    
    @GetMapping(value = "/manageoperations/managerentitems/{operation}")
    @PreAuthorize("hasRole('ROLE_USER')")
    public String managerentItems(@PathVariable Long operation,
            Model model) {

        //Menu Selection
        String menu="userNavBar";
        User currentUser = currentUserFinder.getCurrentUser();
        float total;
        OperationType operationtype;
        boolean operationValidate;
        if(currentUser.getRole().equals("ROLE_USER")){
            menu = "userNavBar";
        }else { menu = "employeeNavBar"; }
        Operation op = operationService.getOperationById(operation); 
         total=op.getTotal();
         operationtype=op.getOperationType();
         operationValidate=op.getValidate();
         List<Item> items = itemService.findByOperation(op);
        model.addAttribute("user",currentUser);
        model.addAttribute("NavBar",menu); // select menu
        model.addAttribute("items", items);
        model.addAttribute("total", total);
        model.addAttribute("operationtype",operationtype );
        model.addAttribute("operationValidate",operationValidate );
        model.addAttribute("operation",operation );
        return "operations/items/manage-rentitems";
    }


    @GetMapping("manageoperations/deleteCartitem/{Id}")
    @PreAuthorize("hasRole('ROLE_USER') or hasRole('ROLE_LIBRARIAN')")
    public String deleteCartItem(@PathVariable Long Id,
    		@RequestParam Long workId ){
    	User currentUser = currentUserFinder.getCurrentUser();
    	Item i=itemService.getItemById(Id);
    	Long operation=i.getOperation().getId();
    	Operation operation1= operationService.findCurentCartOperation(currentUser.getId());
    	List<Item>items=itemService.findByOperation(operation1);
    	
    	float somme=0;
    	
    	if (items.size()>1)
		{//update qte work
            Work work=workService.findById(workId);
            Item item=itemService.getItemById(Id);
            work.setQuantity(work.getQuantity()+item.getQuantity());
            workService.save(work);	
    	//delete cart item
        itemService.deleteItem(Id);
        somme = itemService.SommeItemsCurentOperation(operation1);
        float subTotal= somme;
		double tax=subTotal * 0.18;
    	double total=subTotal +tax;
    	operation1.setIdMembre(currentUser.getId());
    	operation1.setOperationType(OperationType.CART);
    	operation1.setSubTotal((float)subTotal);
    	operation1.setTax((float)tax);
    	operation1.setTotal((float)total);
    	operationService.saveOrUpdate(operation1);
    	return "redirect:/operations/manageoperations/managecartitems/"+operation;}
    	else if(items.size()==1)
		{   //update qte work
            Work work=workService.findById(workId);
            Item item=itemService.getItemById(Id);
            work.setQuantity(work.getQuantity()+item.getQuantity());
            workService.save(work);	
    		//delete item + operation
            itemService.deleteItem(Id);
    		operationService.deleteOperation(operation1.getId());
		return "redirect:/operations/manageoperations1";}
 
    return "redirect:/operations/manageoperations1";    } 
        
    @GetMapping("manageoperations/deleteRentitem/{Id}")
    @PreAuthorize("hasRole('ROLE_USER') or hasRole('ROLE_LIBRARIAN')")
    public String deleteRentItem(@PathVariable Long Id,
    		@RequestParam Long workId ){
    	User currentUser = currentUserFinder.getCurrentUser();
    	Item i=itemService.getItemById(Id);
    	Long operation=i.getOperation().getId();
    	Operation operation1= operationService.findCurentRentOperation(currentUser.getId());
    	List<Item>items=itemService.findByOperation(operation1);
    	
    	float somme=0;
    	
    	if (items.size()>1)
		{//update qte work
            Work work=workService.findById(workId);
            Item item=itemService.getItemById(Id);
            work.setQuantity(work.getQuantity()+item.getQuantity());
            workService.save(work);	
    	//delete cart item
        itemService.deleteItem(Id);
        somme = itemService.SommeItemsCurentOperation(operation1);
        float subTotal= somme;
		double tax=subTotal * 0.18;
    	double total=subTotal +tax;
    	operation1.setIdMembre(currentUser.getId());
    	operation1.setOperationType(OperationType.RENT);
    	operation1.setSubTotal((float)subTotal);
    	operation1.setTax((float)tax);
    	operation1.setTotal((float)total);
    	operationService.saveOrUpdate(operation1);
    	return "redirect:/operations/manageoperations/managerentitems/"+operation;}
    	else if(items.size()==1)
		{   //update qte work
            Work work=workService.findById(workId);
            Item item=itemService.getItemById(Id);
            work.setQuantity(work.getQuantity()+item.getQuantity());
            workService.save(work);	
    		//delete item + operation
            itemService.deleteItem(Id);
    		operationService.deleteOperation(operation1.getId());
		return "redirect:/operations/manageoperations1";}
 
    return "redirect:/operations/manageoperations1";    }
        

    
    @GetMapping(value = "/browseworks")
    @PreAuthorize("hasRole('ROLE_USER')")
    public String browseWorks(
            Model model) {

        //Menu Selection
        String menu="userNavBar";
        User currentUser = currentUserFinder.getCurrentUser();
        if(currentUser.getRole().equals("ROLE_USER")){
            menu = "userNavBar";
        }else { menu = "employeeNavBar"; }

        List<Work> works = workService.findAll();
        model.addAttribute("user",currentUser);
        model.addAttribute("NavBar",menu); // select menu
        model.addAttribute("works", works);
        return "operations/browse-works";
    }
  
//    @GetMapping(value="/browseworksfiltred")
//    @PreAuthorize("hasRole('ROLE_USER')")
//    public String browseWorksFiltred(@RequestParam (required = false) String titre,
//									@RequestParam (required = false) String author,
//									Model model) {
//    	//Menu Selection
//        String menu="userNavBar";
//        User currentUser = currentUserFinder.getCurrentUser();
//        if(currentUser.getRole().equals("ROLE_USER")){
//            menu = "userNavBar";
//        }else { menu = "employeeNavBar"; }
//    	
//        List<Work> works = workService.workSearcher(titre, author);
//        //List<Item> items = itemService.findNonValidateItemCart(currentUser.getId());
//        List<Item> abcd=itemService.getAllItems();
//        System.out.println(abcd.size());
//        model.addAttribute("user",currentUser);
//        model.addAttribute("NavBar",menu); // select menu
//        model.addAttribute("abcd", abcd);
//        model.addAttribute("works", works);
//		model.addAttribute("titre", titre);
//		model.addAttribute("author", author);
//		return "works/browse-works-filtred.html";
//	}
    @PostMapping(value="manageoperations/addcartitem")
	public String addCartItem(
									  @RequestParam Long workId
									  ) {
    	float somme = 0;
    	long IsReserved;
    	User currentUser = currentUserFinder.getCurrentUser();
    	Work work = workService.findById(workId);
		Offre offre=work.getOffre();
		float pourcentage=0;
		int nb=operationService.NBCurentCartOperation(currentUser.getId());
		Date DayDate =  new  Date();
		if (offre!=null && DayDate.after(offre.getStartDate())&& DayDate.before(offre.getEndDate()))
				{ pourcentage=offre.getPercentage()/100;}
    	if (nb==0)
    	{
    	// create new operation + save
    	Operation new_operation=new Operation();
    	new_operation.setIdMembre(currentUser.getId());
    	new_operation.setOperationType(OperationType.CART);
    	float discount=work.getPrice()*pourcentage;
    	new_operation.setSubTotal(work.getPrice()-discount);
    	double tax=(work.getPrice()-discount) * 0.18;
    	double total=work.getPrice()-discount +tax;
    	new_operation.setTax((float)tax);
    	new_operation.setTotal((float)total);
    	Date operationDate =  new  Date();    
        new_operation.setOperationDate( operationDate);
    	operationService.saveOrUpdate(new_operation);
    	//update item
    	Item item =new Item();
		item.setOperation(new_operation);
		item.setWork(work);
		item.setQuantity(1);
		item.setDiscount(discount);
		item.setTotal(work.getPrice()-discount);
		item.setMember(currentUser);
		itemService.saveOrUpdate(item);
    	//update qte work
    	work.setQuantity(work.getQuantity()-1);
    	workService.save(work);
    	}
    	else if (nb==1)
		{
    	Operation operation= operationService.findCurentCartOperation(currentUser.getId());
    	IsReserved=itemService.NBCurentItemWorkOperation(operation,work);
    	if (IsReserved==0)
    	// create new item + save
    	{Item item =new Item();
		item.setOperation(operation);
		item.setWork(work);
		item.setQuantity(1);
		float discount=work.getPrice()*pourcentage;
		item.setDiscount(discount);
		item.setTotal(work.getPrice()-discount);
		item.setMember(currentUser);
		itemService.saveOrUpdate(item);}
    	
    	if (IsReserved==1) {
    	Item item1= itemService.findCurentItemWorkOperation(operation,work);
    		//update quantity+ total item 
            float discount=(work.getPrice())*(item1.getQuantity()+1)*pourcentage;
            item1.setDiscount(discount);
            item1.setTotal((work.getPrice()*(item1.getQuantity()+1))-discount);
            item1.setQuantity(item1.getQuantity()+1);
            itemService.saveOrUpdate(item1);
    	}
		//update qte work
    	work.setQuantity(work.getQuantity()-1);
    	workService.save(work); 
		//update operation
		somme = itemService.SommeItemsCurentOperation(operation);
		float subTotal= somme;
		double tax=subTotal * 0.18;
    	double total=subTotal +tax;
    	Operation operation1= operationService.getOperationById(operation.getId());
    	operation1.setIdMembre(currentUser.getId());
    	operation1.setOperationType(OperationType.CART);
    	operation1.setSubTotal((float)subTotal);
    	operation1.setTax((float)tax);
    	operation1.setTotal((float)total);
    	operation1.setId(operation.getId());
    	agent.save(operation1);
    	
		}
        return "redirect:/book";
	}
   
    @PostMapping(value="manageoperations/addrentitem")
	public String addRentItem(
									  @RequestParam Long workId
									  ) {
    	float somme = 0;
    	long IsReserved;
    	User currentUser = currentUserFinder.getCurrentUser();
    	Work work = workService.findById(workId);
    	Offre offre=work.getOffre();
		float pourcentage=0;
		Date DayDate =  new  Date();
		if (offre!=null && DayDate.after(offre.getStartDate())&& DayDate.before(offre.getEndDate()))
				{ pourcentage=offre.getPercentage()/100;}
    	int nb=operationService.NBCurentRentOperation(currentUser.getId());
    	
    	if (nb==0)
    	{	
    	// create new operation + save
    	Operation new_operation=new Operation();
    	new_operation.setIdMembre(currentUser.getId());
    	new_operation.setOperationType(OperationType.RENT);
    	float discount=work.getPrice()*pourcentage;
    	new_operation.setSubTotal((work.getPrice()-discount)*3);
    	double tax=(work.getPrice()-discount)*3 * 0.18;
    	double total=(work.getPrice()-discount)*3 +tax;
    	new_operation.setTax((float)tax);
    	new_operation.setTotal((float)total);
    	Date operationDate =  new  Date();    
        new_operation.setOperationDate( operationDate);
    	operationService.saveOrUpdate(new_operation);
    	//update item
    	Item item =new Item();
		item.setOperation(new_operation);
		item.setWork(work);
		item.setQuantity(1);
		item.setDiscount(discount);
		item.setTotal((work.getPrice()-discount)*3);
		Date operationRentDate =  new  Date();
		Date operationRentDate1=new Date(operationRentDate.getTime() + (1000 * 60 * 60 * 24)*2);
        item.setStartDate(operationRentDate1);
        item.setNbDays(3);
        item.setMember(currentUser);
        Date operationExpireDate1=new Date(operationRentDate1.getTime() + (1000 * 60 * 60 * 24)*3);
        item.setExpireDate(operationExpireDate1);
        itemService.saveOrUpdate(item);
    	//update qte work
    	work.setQuantity(work.getQuantity()-1);
     	workService.save(work);
    	}
    	else if (nb==1)
		{
    	Operation operation= operationService.findCurentRentOperation(currentUser.getId());
    	IsReserved=itemService.NBCurentItemWorkOperation(operation,work);
    	if (IsReserved==0)
    	// create new item + save
    	{Item item =new Item();
    	float discount=work.getPrice()*pourcentage;
    	item.setOperation(operation);
		item.setWork(work);
		item.setQuantity(1);
		item.setDiscount(discount);
		item.setTotal((work.getPrice()-discount)*3);
		Date operationRentDate =  new  Date();
		Date operationRentDate1=new Date(operationRentDate.getTime() + (1000 * 60 * 60 * 24)*2);
        item.setStartDate(operationRentDate1);
        item.setNbDays(3);
        item.setMember(currentUser);
        Date operationExpireDate1=new Date(operationRentDate1.getTime() + (1000 * 60 * 60 * 24)*3);
        item.setExpireDate(operationExpireDate1);
        itemService.saveOrUpdate(item);
      //update qte work
    	work.setQuantity(work.getQuantity()-1);
    	workService.save(work); 
		//update operation
		somme = itemService.SommeItemsCurentOperation(operation);
		float subTotal= somme;
		double tax=subTotal * 0.18;
    	double total=subTotal +tax;
    	Operation operation1= operationService.getOperationById(operation.getId());
    	operation1.setIdMembre(currentUser.getId());
    	operation1.setOperationType(OperationType.RENT);
    	operation1.setSubTotal((float)subTotal);
    	operation1.setTax((float)tax);
    	operation1.setTotal((float)total);
    	operation1.setId(operation.getId());
    	agent.save(operation1);}
    	
    	if (IsReserved==1) {

    	}
		
    	
		}
        return "redirect:/book";
	}
    
    
    
    @PreAuthorize("hasRole('ROLE_USER')")
    @GetMapping(value="/manageoperations/updateCartitem/{Id}")
	public String updateCartitem(@PathVariable Long Id,Model model) {
    	//Menu Selection
        String menu="userNavBar";
        User currentUser = currentUserFinder.getCurrentUser();
        if(currentUser.getRole().equals("ROLE_USER")){
            menu = "userNavBar";
        }else { menu = "employeeNavBar"; }
    	Item item=itemService.getItemById(Id);
		String titre=itemService.getItemById(Id).getWork().getTitle();
    	model.addAttribute("item",item );
    	model.addAttribute("titre",titre );
    	
		return "operations/items/update-cartitem.html";
	}
    @PreAuthorize("hasRole('ROLE_USER')")
    @GetMapping(value="/manageoperations/updateRentitem/{Id}")
	public String updateRentitem(@PathVariable Long Id,Model model) {
    	//Menu Selection
        String menu="userNavBar";
        User currentUser = currentUserFinder.getCurrentUser();
        if(currentUser.getRole().equals("ROLE_USER")){
            menu = "userNavBar";
        }else { menu = "employeeNavBar"; }
    	Item item=itemService.getItemById(Id);
		String titre=itemService.getItemById(Id).getWork().getTitle();
    	model.addAttribute("item",item );
    	model.addAttribute("titre",titre );
    	
		return "operations/items/update-rentitem.html";
	}
    @PostMapping(value="/manageoperations/saveCartitem")
    @PreAuthorize("hasRole('ROLE_USER')")
    public String updatedCartInfos(@RequestParam int itemNewQte,
                                    @RequestParam Long itemId,
                                    @RequestParam Long workId) {
        
    	float somme=0; 
    	Item item = itemService.getItemById(itemId);
      //Menu Selection
        String menu="userNavBar";
        User currentUser = currentUserFinder.getCurrentUser();
        if(currentUser.getRole().equals("ROLE_USER")){
            menu = "userNavBar";
        }else { menu = "employeeNavBar"; }
        //update qte work
        Work work=workService.findById(workId);
        Offre offre=work.getOffre();
		float pourcentage=0;
		Date DayDate =  new  Date();
		if (offre!=null && DayDate.after(offre.getStartDate())&& DayDate.before(offre.getEndDate()))
				{ pourcentage=offre.getPercentage()/100;}
		float discount=work.getPrice()*pourcentage*itemNewQte;
		work.setQuantity(work.getQuantity()-itemNewQte+item.getQuantity());
        workService.save(work);
        //update quantity+ total item
        item.setQuantity(itemNewQte);
        item.setDiscount(discount);
        item.setTotal(itemNewQte*item.getWork().getPrice()-discount);
        itemService.saveOrUpdate(item);
      
       //update operation 
        Operation operation=item.getOperation();
        somme = itemService.SommeItemsCurentOperation(operation);
		float subTotal= somme;
		double tax=subTotal * 0.18;
    	double total=subTotal +tax;
    	operation.setSubTotal((float)subTotal);
    	operation.setTax((float)tax);
    	operation.setTotal((float)total);
    	agent.save(operation);
        return "redirect:/operations/manageoperations/managecartitems/"+item.getOperation().getId();
    }
    @PostMapping(value="/manageoperations/saveRentitem")
    @PreAuthorize("hasRole('ROLE_USER')")
    public String updatedRentInfos(@RequestParam int itemNewQte,
                                    @RequestParam Long itemId,
                                    @RequestParam Long workId,
                                    @RequestParam Long nbdaysNew,
                                    @DateTimeFormat(pattern = "yyyy-MM-dd") @RequestParam Date startNew) {
        
    	float somme=0; 
    	Item item = itemService.getItemById(itemId);
      //Menu Selection
        String menu="userNavBar";
        User currentUser = currentUserFinder.getCurrentUser();
        if(currentUser.getRole().equals("ROLE_USER")){
            menu = "userNavBar";
        }else { menu = "employeeNavBar"; }
        //update qte work
        Work work=workService.findById(workId);
        Offre offre=work.getOffre();
		float pourcentage=0;
		Date DayDate =  new  Date();
		if (offre!=null && DayDate.after(offre.getStartDate())&& DayDate.before(offre.getEndDate()))
				{ pourcentage=offre.getPercentage()/100;}
		float discount=work.getPrice()*pourcentage*itemNewQte*nbdaysNew;
        work.setQuantity(work.getQuantity()-itemNewQte+item.getQuantity());
        workService.save(work);
        //update quantity+ total item
        item.setQuantity(itemNewQte);
        item.setTotal(itemNewQte*item.getWork().getPrice()*nbdaysNew-discount);
        item.setDiscount(discount);
        item.setStartDate(startNew);
        item.setNbDays(nbdaysNew);
        Date operationExpireNewDate=new Date(startNew.getTime() + (1000 * 60 * 60 * 24)*nbdaysNew);
        item.setExpireDate(operationExpireNewDate);
        itemService.saveOrUpdate(item);
      
       //update operation 
        Operation operation=item.getOperation();
        somme = itemService.SommeItemsCurentOperation(operation);
		float subTotal= somme;
		double tax=subTotal * 0.18;
    	double total=subTotal +tax;
    	operation.setSubTotal((float)subTotal);
    	operation.setTax((float)tax);
    	operation.setTotal((float)total);
    	agent.save(operation);
        return "redirect:/operations/manageoperations/managerentitems/"+item.getOperation().getId();
    }
    @PreAuthorize("hasRole('ROLE_LIBRARIAN')")
    @GetMapping(value="/returnedworks")
	public String returnedWorks(
								@RequestParam (required = false) String firstname,
								@RequestParam (required = false) String lastname,
																Model model) {
    	//Menu Selection
        String menu="userNavBar";
        User currentUser = currentUserFinder.getCurrentUser();
        if(currentUser.getRole().equals("ROLE_USER")){
            menu = "userNavBar";
        }else { menu = "employeeNavBar"; }
		
		
        List<User> users = usService.userSearcher(firstname, lastname);
		model.addAttribute("user",currentUser);
	    model.addAttribute("NavBar",menu); // select menu
		model.addAttribute("users", users);
		return "operations/librarian-returned-works.html";
	}
    @GetMapping(value = "returnedworks/alluserworks/{userid}")
    @PreAuthorize("hasRole('ROLE_LIBRARIAN')")
    public String alluserworks(@PathVariable Long userid,
            Model model) {

        //Menu Selection
        String menu="userNavBar";
        User currentUser = currentUserFinder.getCurrentUser();
        if(currentUser.getRole().equals("ROLE_USER")){
            menu = "userNavBar";
        }else { menu = "employeeNavBar"; }
        
        User u=usService.findById(userid);
        List<Item> items = itemService.findByMember(u);
        model.addAttribute("user",currentUser);
        model.addAttribute("NavBar",menu); // select menu
        model.addAttribute("items", items);
        return "operations/librarian-alluserworks";
    }
    @PreAuthorize("hasRole('ROLE_LIBRARIAN')")
    @GetMapping(value="/returnedworks/confirmreturnedworks")
	public String confirmReturnedWorks(@RequestParam Long workId,
									   @RequestParam Long userId,
									   @RequestParam Long itemId,
									   Model model) {
		
					
		model.addAttribute("item", itemService.getItemById(itemId));
		model.addAttribute("itemid", itemId);
		model.addAttribute("itemid", workId);
		model.addAttribute("user", usService.findById(userId));
		return "operations/librarian-confirm_retruned_works.html";
	}
    @PostMapping(value="/returnedworks/isReturned")
    @PreAuthorize("hasRole('ROLE_LIBRARIAN')")
    public String isReturned( @RequestParam Long itemId,
    		                  @RequestParam Long workId,
    		                  Model model) {
		String menu="userNavBar";
        User currentUser = currentUserFinder.getCurrentUser();
        if(currentUser.getRole().equals("ROLE_USER")){
            menu = "userNavBar";
        }else { menu = "employeeNavBar"; }
		Item item=itemService.getItemById(itemId);
		Work work=workService.findById(workId);
		item.setReturned(true);
		Date returnDate =  new  Date();    
        item.setReturnDate( returnDate);
        itemService.saveOrUpdate(item);
      //update qte work
    	work.setQuantity(work.getQuantity()+1);
    	workService.save(work);
		model.addAttribute("user", item.getMember());
		return "operations/librarian-isreturned.html";
	}
    
}
