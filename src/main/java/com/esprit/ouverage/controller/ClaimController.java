package com.esprit.ouverage.controller;

import com.esprit.ouverage.config.CurrentUserFinder;
import com.esprit.ouverage.model.Claim;
import com.esprit.ouverage.model.ClaimSubject;
import com.esprit.ouverage.model.StatusClaim;
import com.esprit.ouverage.model.User;
import com.esprit.ouverage.service.ClaimService;
import com.esprit.ouverage.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.annotation.Secured;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;

import java.util.ArrayList;
import java.util.List;

@Controller
@RequestMapping("/claims")
public class ClaimController {



    @Autowired
    ClaimService claimService;
    @Autowired
    UserService usService;
    @Autowired
    CurrentUserFinder currentUserFinder;

//    private User currentUser = currentUserFinder.getCurrentUser();



    // GET All Claims byStatus
    // TODO: add other filters (byDate, byUser)

    @GetMapping(value = "/manageclaims")
    @PreAuthorize("hasRole('ROLE_LIBRARIAN')")
    public String manageClaims(
            @RequestParam (required = false) StatusClaim claimStat,
            @RequestParam (required = false) String pseudo,
            Model model) {

        //Menu Selection
        String menu="employeeNavBar";
        List<Claim> claims = claimService.claimSearch(claimStat, pseudo);;

        User currentUser = currentUserFinder.getCurrentUser();
        Long currentUserId = currentUserFinder.getCurrentUserId();

        // todo: Return two lists one for user, and one for librarian
        model.addAttribute("status",claimStat);
        model.addAttribute("pseudo",pseudo);
        model.addAttribute("user",currentUser);
        model.addAttribute("NavBar",menu); // select menu
        model.addAttribute("claims", claims);
        return "Claims/manage-claims";
    }

    @GetMapping(value = "/useranageclaims")
    @PreAuthorize("hasRole('ROLE_USER')")
    public String usermanageClaims(
            @RequestParam (required = false) StatusClaim claimStat,
            Model model) {

        User currentUser = currentUserFinder.getCurrentUser();
        Long currentUserId = currentUserFinder.getCurrentUserId();

        //Menu Selection
        String menu="userNavBar";
        List<Claim> claims = claimService.getClaimsbyUser_(claimStat,currentUserId);

        // todo: Return two lists one for user, and one for librarian
        model.addAttribute("status",claimStat);
        model.addAttribute("user",currentUser);
        model.addAttribute("NavBar",menu); // select menu
        model.addAttribute("claims", claims);
        return "Claims/user-manage-claims";
    }

    // MODIFY Claim Infos

    @GetMapping(value="/manageclaim")
    @PreAuthorize("hasRole('ROLE_LIBRARIAN') or hasRole('ROLE_USER')")
    public String manageClaim(@RequestParam Long claimId,
                                Model model) {
        User currentUser = currentUserFinder.getCurrentUser();
        Claim claim = claimService.getClaimById(claimId);
        model.addAttribute("claim", claim);
//        model.addAttribute("statusclaim", StatusClaim.values());
        model.addAttribute("currentUser", currentUser);
        return "Claims/manage-claim";
    }


    // UPDATE Claim Status (LIBRARIAN)
    @PostMapping(value="/claiminfosaved")
    @PreAuthorize("hasRole('ROLE_LIBRARIAN')")
    public String updatedClaimStatus(@RequestParam StatusClaim claimStatus,
                                      @RequestParam Long claimId) {
        Claim claim = claimService.getClaimById(claimId);
        claim.setStatus(claimStatus);
        claimService.saveAndUpdate(claim);
        return "redirect:/claims/manageclaims";
    }


    // UPDATE Claim Infos USER

    @PostMapping(value="/claiminfomodify")
    @PreAuthorize("hasRole('ROLE_USER')")
    public String updatedClaimInfos(@RequestParam String claimCon,
                                    @RequestParam ClaimSubject claimSub,
                                    @RequestParam Long claimId) {
        Claim claim = claimService.getClaimById(claimId);
        claim.setContent(claimCon);
        claim.setSubject(claimSub);
        claimService.saveAndUpdate(claim);
        return "redirect:/claims/useranageclaims";
    }

    // ADD Claim (USER)
    @GetMapping("/addclaim")
    @PreAuthorize("hasRole('ROLE_USER')")
    public String addClaim (Model model){
        Claim newClaim = new Claim();
        model.addAttribute("newclaim", newClaim);
        return "Claims/add-claim";
    }

    // SAVE/Persist Claim (USER)
    @PostMapping("/saveclaim")
    @PreAuthorize("hasRole('ROLE_USER')")
    public String SaveClaims(Claim newClaim){
        newClaim.setUserId(currentUserFinder.getCurrentUser());
        newClaim.setStatus(StatusClaim.PENDING);
        claimService.saveAndUpdate(newClaim);
        return "redirect:/claims/useranageclaims";
    }


    // DELETE Claim (User)
    @GetMapping("/deleteclaim")
    @PreAuthorize("hasRole('ROLE_USER')")
    public String deleteClaim(@RequestParam Long claimId){
        claimService.deleteClaim(claimId);
        return "redirect:/claims/useranageclaims";
    }

}
