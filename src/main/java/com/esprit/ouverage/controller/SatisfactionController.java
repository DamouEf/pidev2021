package com.esprit.ouverage.controller;

import com.esprit.ouverage.config.CurrentUserFinder;
import com.esprit.ouverage.model.*;
import com.esprit.ouverage.service.OperationServiceImpl;
import com.esprit.ouverage.service.SatisfactionService;
import com.esprit.ouverage.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.annotation.Secured;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.stream.Collectors;

@Controller
@RequestMapping("/satisfactions")
public class SatisfactionController {

    @Autowired
    SatisfactionService satisfactionService;
    @Autowired
    UserService usService;
    @Autowired
    OperationServiceImpl operationService;
    @Autowired
    CurrentUserFinder currentUserFinder;

    @GetMapping(value = "/managesatisfactions")
    @PreAuthorize("hasRole('ROLE_LIBRARIAN') or hasRole('ROLE_USER')")
    public String manageSatisfactions(
            Model model) {

        //Menu Selection
        String menu="userNavBar";
        User currentUser = currentUserFinder.getCurrentUser();
        List<Satisfaction> satisfactions;
        if(currentUser.getRole().equals("ROLE_USER")){
            menu = "userNavBar";
            satisfactions = satisfactionService.getAllSatisfaction().stream().filter(satisfaction -> satisfaction.getUserId().getId().equals(currentUser.getId())).collect(Collectors.toList());
        }else {
            menu = "employeeNavBar";
            satisfactions = satisfactionService.getAllSatisfaction();
        }

        model.addAttribute("user",currentUser);
        model.addAttribute("NavBar",menu); // select menu
        model.addAttribute("satisfactions", satisfactions);
        return "Satisfactions/manage-satisfactions";
    }


    // ADD Satisfaction (USER)
    @GetMapping("/addsatisfaction")
    @PreAuthorize("hasRole('ROLE_LIBRARIAN') or hasRole('ROLE_USER')")
    public String addSatisfaction (@RequestParam Long operationId,Model model){
        Operation operation = operationService.getOperationById(operationId);
        Satisfaction newSatisfaction = new Satisfaction();
        newSatisfaction.setOperationId(operation);
        model.addAttribute("newsatisfaction", newSatisfaction);
        return "Satisfactions/add-satisfaction";
    }

    // SAVE/Persist Satisfaction (USER)
    @PostMapping("/savesatisfaction/{operationId}")
    @PreAuthorize("hasRole('ROLE_LIBRARIAN') or hasRole('ROLE_USER')")
    public String SaveSatisfactions(@PathVariable("operationId") long operationId ,Satisfaction newSatisfaction){

        Operation operation = operationService.getOperationById(operationId);
        newSatisfaction.setOperationId(operation);
        newSatisfaction.setUserId(currentUserFinder.getCurrentUser());
        satisfactionService.saveAndUpdate(newSatisfaction);
        return "redirect:/satisfactions/managesatisfactions";
    }



    // MODIFY Satisfaction Infos
    @GetMapping(value="/managesatisfaction")
    @PreAuthorize("hasRole('ROLE_LIBRARIAN') or hasRole('ROLE_USER')")
    public String manageSatisfaction(@RequestParam Long satisfactionId,
                              Model model) {
        User currentUser = currentUserFinder.getCurrentUser();
        Satisfaction satisfaction = satisfactionService.getSatisfactionById(satisfactionId);
        model.addAttribute("satisfaction", satisfaction);
        model.addAttribute("currentUser", currentUser);
        return "Satisfactions/manage-satisfaction";
    }


    // UPDATE Satisfaction Infos USER

    @PostMapping(value="/satisfactioninfomodify")
    @PreAuthorize("hasRole('ROLE_LIBRARIAN') or hasRole('ROLE_USER')")
    public String updatedSatisfactionInfos(@RequestParam String satisfactionNewMessage,
                                    //@RequestParam Integer satisfactionNewRating,
                                    @RequestParam Long satisfactionId) {
        Satisfaction satisfaction = satisfactionService.getSatisfactionById(satisfactionId);
        satisfaction.setMessage(satisfactionNewMessage);
        //satisfaction.setRating(rate);
        satisfactionService.saveAndUpdate(satisfaction);
        return "redirect:/satisfactions/managesatisfactions";
    }


    // DELETE Satisfaction (User)
    @GetMapping("/deletesatisfaction")
    @PreAuthorize("hasRole('ROLE_USER') or hasRole('ROLE_LIBRARIAN')")
    public String deleteSatisfaction(@RequestParam Long satisfactionId){
        satisfactionService.deleteSatisfaction(satisfactionId);
        return "redirect:/satisfactions/managesatisfactions";
    }

}
