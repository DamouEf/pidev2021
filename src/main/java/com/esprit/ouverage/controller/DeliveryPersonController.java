package com.esprit.ouverage.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;
import com.esprit.ouverage.config.CurrentUserFinder;
import com.esprit.ouverage.model.DeliveryPerson;
import com.esprit.ouverage.model.User;
import com.esprit.ouverage.repository.DeliveryPersonRepository;
import com.esprit.ouverage.service.UserService;
import com.esprit.ouverage.service.DeliveryPersonService;

@Controller
@RequestMapping("/DeliveryPerson")
public class DeliveryPersonController {
	
        @Autowired
        DeliveryPersonService DeliveryPersonService;
		@Autowired
	    UserService usService;
	    @Autowired
	    CurrentUserFinder currentUserFinder;

	 // list all  delivery persons 
	    
	    @GetMapping(value = "/manageDeliveryPersons")
	    @PreAuthorize("hasRole('ROLE_LIBRARIAN')")
	    public String manageDeliveryPersons(
	            
	            Model model) {

	        //Menu Selection
	        String menu="userNavBar";
	        User currentUser = currentUserFinder.getCurrentUser();
	        if(currentUser.getRole().equals("ROLE_USER")){
	             menu = "userNavBar";
	        }else { menu = "employeeNavBar"; }


	        List<DeliveryPerson> DeliveryPersons = DeliveryPersonService.getAllDeliveryPerson();
	        model.addAttribute("user",currentUser);
	        model.addAttribute("NavBar",menu); // select menu
	        model.addAttribute("DeliveryPersons", DeliveryPersons);
	        return "DeliveryPerson/list-DeliveryPerson";
	    }
	    
	 // list all  delivery persons  for a user
	    
	    @GetMapping(value = "/manageDeliveryPersonsUser")
	    @PreAuthorize("hasRole('ROLE_USER') or hasRole('ROLE_LIBRARIAN')")
	    public String manageDeliveryPersonsUser(
	            
	            Model model) {

	        //Menu Selection
	        String menu="userNavBar";
	        User currentUser = currentUserFinder.getCurrentUser();
	        if(currentUser.getRole().equals("ROLE_USER")){
	             menu = "userNavBar";
	        }else { menu = "employeeNavBar"; }


	        List<DeliveryPerson> DeliveryPersons = DeliveryPersonService.getAllDeliveryPerson();
	        model.addAttribute("user",currentUser);
	        model.addAttribute("NavBar",menu); // select menu
	        model.addAttribute("DeliveryPersons", DeliveryPersons);
	        return "DeliveryPerson/userlist-DeliveryPerson";
	    }

	   // delete a delivery person then redirect to the list
	    
	    
	    @GetMapping("/deleteDeliveryPerson/{id}")
	    @PreAuthorize("hasRole('ROLE_USER') or hasRole('ROLE_LIBRARIAN')")
	    public String deleteDeliveryPerson(@PathVariable Long id){
	    	DeliveryPersonService.deleteDeliveryPerson(id);
	        return "redirect:/DeliveryPerson/manageDeliveryPersons";
	    }
// add a delivery person then redirect to the list
	    
	    @GetMapping("/addDeliveryPerson")
	    @PreAuthorize("hasRole('ROLE_USER') or hasRole('ROLE_LIBRARIAN')")
	    public String addDeliveryPerson (Model model){
	    	DeliveryPerson newDeliveryPerson = new DeliveryPerson();
	        model.addAttribute("newDeliveryPerson", newDeliveryPerson);
	        return "DeliveryPerson/add-DeliveryPerson";
	    }

	    // SAVE/Persist DeliveryPerson 
	    @PostMapping("/saveDeliveryPerson")
	    @PreAuthorize("hasRole('ROLE_USER') or hasRole('ROLE_LIBRARIAN')")
	    public String SaveDeliveryPersons(DeliveryPerson newDeliveryPerson){
	        DeliveryPersonService.saveOrUpdate(newDeliveryPerson);
	        return "redirect:/DeliveryPerson/manageDeliveryPersons";
	    }
	    
	    

			  
			    @GetMapping("/modifyDeliveryPerson/{id}")
			    @PreAuthorize("hasRole('ROLE_USER') or hasRole('ROLE_LIBRARIAN')")
			    public String update(@PathVariable("id") long id , Model model) {
			    	DeliveryPerson deliveryPerson =  DeliveryPersonService.getDeliveryPersonById(id);
			        model.addAttribute("newDeliveryPerson", deliveryPerson);
			        return "DeliveryPerson/modify-DeliveryPerson";
			    }
			    @PostMapping("/modifyDeliveryPerson/{id}")
			    @PreAuthorize("hasRole('ROLE_USER') or hasRole('ROLE_LIBRARIAN')")
			    public String SavenewDeliveryPerson(@PathVariable("id") long id ,DeliveryPerson newDeliveryPerson,BindingResult result, Model model){
			    	DeliveryPerson oldDeliveryPerson =  DeliveryPersonService.getDeliveryPersonById(id);
			    	oldDeliveryPerson.setName(newDeliveryPerson.getName());
			    	oldDeliveryPerson.setSurname(newDeliveryPerson.getSurname());
			    	oldDeliveryPerson.setPhone(newDeliveryPerson.getPhone());
			    	oldDeliveryPerson.setRating(newDeliveryPerson.getRating());
                DeliveryPersonService.saveOrUpdate(newDeliveryPerson);
			        return "redirect:/DeliveryPerson/manageDeliveryPersons";
			    }

		  
		  
		}
