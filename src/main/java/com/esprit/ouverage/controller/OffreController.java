package com.esprit.ouverage.controller;

import java.util.Date;

import java.util.List;



import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import com.esprit.ouverage.model.Offre;
import com.esprit.ouverage.repository.OffreRepository;
import com.esprit.ouverage.service.OffreService;

@Controller
@RequestMapping(path = "/lib")
public class OffreController {

	
	  @Autowired
	    OffreRepository offerRepo;
	    @GetMapping
	    @PreAuthorize("hasRole('ROLE_LIBRARIAN')")
	    public String librarianHome(Model model) {
			   List<Offre> promotionList = offerRepo.findAll();
		        model.addAttribute("promotionList", promotionList);
	        return "Librarian/promotion";
	    }
	    
	    @GetMapping("/delete/{id}")
	    public String deleteOffre(@PathVariable("id") long id, Model model) {
	    	Offre offer = offerRepo.findById(id)
	          .orElseThrow(() -> new IllegalArgumentException("Invalid offer Id:" + id));
	    	offerRepo.delete(offer);
	        return "redirect:/lib";
	    }
	    
	    @GetMapping("/edit/{id}")
	    public String showUpdateForm(@PathVariable("id") long id, Model model) {
	    	Offre offer = offerRepo.findById(id).orElseThrow(() -> new IllegalArgumentException("Invalid offer Id:" + id));
	        model.addAttribute("promotion", offer);
	        
	        return "Librarian/update-promotion";
	    }
	    
	    @PostMapping("/update/{id}")
	    public String updateOffre(@PathVariable("id") long id, Offre offer, BindingResult result, Model model) {
	        if (result.hasErrors()) {
	        	offer.setId(id);
	            return "Librarian/update-promotion";
	        }
	        offerRepo.save(offer);
	        return "redirect:/lib";
	    }
	    
	    @GetMapping("/addpromotion")
	    public String addPromotion( Model model) {
	    	Offre offer = new Offre();
	        model.addAttribute("promotion", offer);
	        return "Librarian/add-promotion";
	    }
	    
	    
	    @PostMapping("/save")
	    public String addOffre(@ModelAttribute(value ="promotion") Offre offer, BindingResult result,Model model) {
	        if (result.hasErrors()) {
	            return "Librarian/add-promotion";
	        }
	    	System.out.println(offer.getPercentage());
	    	
	        offerRepo.save(offer);
	        return "redirect:/lib";
	    }
	   
}
