package com.esprit.ouverage.controller;

import com.esprit.ouverage.config.CurrentUserFinder;
import com.esprit.ouverage.model.User;
import com.esprit.ouverage.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestMapping;

@Controller
@RequestMapping(path = "/user")
public class UserController {


    @Autowired
    UserService usService;

    @Autowired
    CurrentUserFinder currentUserFinder;


    @GetMapping
    @PreAuthorize("hasRole('ROLE_USER')")
    public String userHome(Model model) {
        User currentUser = currentUserFinder.getCurrentUser();
        model.addAttribute("currentUser", currentUser);
        return "Member/user-home";
    }



}
