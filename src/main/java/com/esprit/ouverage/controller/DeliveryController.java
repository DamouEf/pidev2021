package com.esprit.ouverage.controller;

import java.util.List;
import java.util.stream.Collectors;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;

import com.esprit.ouverage.config.CurrentUserFinder;
import com.esprit.ouverage.model.Delivery;
import com.esprit.ouverage.model.DeliveryPerson;
import com.esprit.ouverage.model.Operation;
import com.esprit.ouverage.model.User;
import com.esprit.ouverage.service.DeliveryPersonService;
import com.esprit.ouverage.service.DeliveryService;
import com.esprit.ouverage.service.OperationService;
import com.esprit.ouverage.service.UserService;

@Controller
@RequestMapping("/Delivery")
public class DeliveryController {
    @Autowired
    DeliveryService DeliveryService;
	@Autowired
    UserService usService;
    @Autowired
    CurrentUserFinder currentUserFinder;
    @Autowired
    DeliveryPersonService deliveryService;
    @Autowired
    OperationService operationService;
    
	 // list all  delivery persons 
    
    @GetMapping(value = "/manageDeliverys")
    @PreAuthorize("hasRole('ROLE_LIBRARIAN') or hasRole('ROLE_USER')")
    public String manageDeliverys(Model model) {

        String menu="userNavBar";
        User currentUser = currentUserFinder.getCurrentUser();
        if(currentUser.getRole().equals("ROLE_USER")){
             menu = "userNavBar";
        }else { menu = "employeeNavBar"; }
        List<Delivery> Deliverys = DeliveryService.getAllDelivery();
        model.addAttribute("user",currentUser);
        model.addAttribute("NavBar",menu); // select menu
        model.addAttribute("Deliverys", Deliverys);
        return "Delivery/list-Delivery";
    }
    // list all deliveries for current user
    
    
    @GetMapping(value = "/UsermanageDeliverys")
    @PreAuthorize("hasRole('ROLE_USER')")
    public String UsermanageDeliverys(Model model) {


        String menu="userNavBar";
       // User currentUser = currentUserFinder.getCurrentUser();
        long user = currentUserFinder.getCurrentUserId();
        List<Delivery> userDelivery = DeliveryService.getAllDelivery().stream().filter(d->d.getOperation().getIdMembre() == user).collect(Collectors.toList());
       // model.addAttribute("user",currentUser);
        model.addAttribute("NavBar",menu); // select menu
        model.addAttribute("userDelivery", userDelivery);
        return "Delivery/userlist-delivery";
    }

   // delete a delivery  then redirect to the list
    
	
	  @GetMapping("/deleteDelivery/{idDelivery}")
	  
	  @PreAuthorize("hasRole('ROLE_USER') or hasRole('ROLE_LIBRARIAN')") 
	  public String deleteDelivery(@PathVariable Long idDelivery){
		  
	  DeliveryService.deleteDelivery(idDelivery);
	  return "redirect:/Delivery/manageDeliverys";
	  }

	// add a delivery  then redirect to the list
	    
	    @GetMapping("/addDelivery")
	    @PreAuthorize("hasRole('ROLE_USER') or hasRole('ROLE_LIBRARIAN')")
	    public String addDelivery (Model model){
	    	Delivery newDelivery = new Delivery();
	        model.addAttribute("newDelivery", newDelivery);
	        List <DeliveryPerson> deliveryPersons =deliveryService.getAllDeliveryPerson();
	        List<Operation> Operations= operationService.getAllOperations();
	        model.addAttribute("Operations",Operations);
	        model.addAttribute("deliveryPs",deliveryPersons);
	        return "Delivery/add-Delivery";
	    }

	    // SAVE/Persist Delivery 
	    @PostMapping("/saveDelivery")
	    @PreAuthorize("hasRole('ROLE_USER') or hasRole('ROLE_LIBRARIAN')")
	    
	    public String SaveDeliverys(Delivery newDelivery){
	        DeliveryService.saveOrUpdate(newDelivery);
	        return "redirect:/Delivery/manageDeliverys";
	    }
	    
	    //modify delivery informations
	    
	    
	    @GetMapping("/modifyDelivery/{idDelivery}")
	    @PreAuthorize("hasRole('ROLE_USER') or hasRole('ROLE_LIBRARIAN')")
	    public String update(@PathVariable("idDelivery") long idDelivery , Model model) {
	    	Delivery delivery =  DeliveryService.getDeliveryById(idDelivery);
	    	List <DeliveryPerson> deliveryPersons =deliveryService.getAllDeliveryPerson();
	        List<Operation> Operations= operationService.getAllOperations();
	        model.addAttribute("Operations",Operations);
	        model.addAttribute("deliveryPs",deliveryPersons);
	        model.addAttribute("newDelivery", delivery);
	        return "Delivery/modify-Delivery";
	    }
	    @PostMapping("/modifyDelivery/{idDelivery}")
	    @PreAuthorize("hasRole('ROLE_USER') or hasRole('ROLE_LIBRARIAN')")
	    public String SavenewDelivery(@PathVariable("idDelivery") long idDelivery ,Delivery newDelivery,BindingResult result, Model model){
	    	Delivery oldDelivery =  DeliveryService.getDeliveryById(idDelivery);
	    	
	    	
	    	oldDelivery.setDeliveryDate(newDelivery.getDeliveryDate());
        
	    	oldDelivery.setOperation(newDelivery.getOperation());
	    	oldDelivery.setPerson(newDelivery.getPerson());
	    	oldDelivery.setSvc(newDelivery.getSvc());

        DeliveryService.saveOrUpdate(newDelivery);
	        return "redirect:/Delivery/manageDeliverys";
	    }
	    
	    
	    
	    
	    
	    
	    
	    
	    
	 
}
