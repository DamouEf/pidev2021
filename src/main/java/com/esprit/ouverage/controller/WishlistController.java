package com.esprit.ouverage.controller;

import com.esprit.ouverage.config.CurrentUserFinder;
import com.esprit.ouverage.model.User;
import com.esprit.ouverage.model.WishList;
import com.esprit.ouverage.model.Work;
import com.esprit.ouverage.repository.WorkRepository;
import com.esprit.ouverage.service.UserService;
import com.esprit.ouverage.service.WishlistService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;

import java.util.List;

@Controller
@RequestMapping(path = "/wishlists")
@PreAuthorize("hasRole('ROLE_USER')")
public class WishlistController {

    @Autowired
    WorkRepository workagent;
    @Autowired
    WishlistService wishlistService;
    @Autowired
    UserService usService;
    @Autowired
    CurrentUserFinder currentUserFinder;


    @GetMapping("/mywishlist")
    public String getMyWishlist(
            Model model
    ){

        String menu="userNavBar";
        User currentUser = currentUserFinder.getCurrentUser();
        if(currentUser.getRole().equals("ROLE_USER")){
            menu = "userNavBar";
        }else { menu = "employeeNavBar"; }


        Long currentUserId = currentUserFinder.getCurrentUserId();
//        User currentUser = currentUserFinder.getCurrentUser();

        List <WishList> myWishlist = wishlistService.getMyWishlist(currentUserId);


        model.addAttribute("user", currentUser);
        model.addAttribute("mywishlist", myWishlist);
        model.addAttribute("NavBar", menu);
        return "Wishlists/view-wishlist";
    }


    @GetMapping("/removewishlist")
    public String removeFromWishlist(
            @RequestParam Long wishlistId
    ){
        wishlistService.deleteWishlist(wishlistId);
        return "redirect:/wishlists/mywishlist";
    }

    @PostMapping("/savewishlist")
    public String saveWishList(
            WishList newWishlist,
            @RequestParam Long bookId
    ) {
        //get userId from id
        User currentUser = currentUserFinder.getCurrentUser();
        Long currentUserId = currentUserFinder.getCurrentUserId();

        List<WishList> userWishlist = wishlistService.getMyWishlist(currentUserId);


        //get book
        Work book = workagent.findById(bookId).get();

        // search if the book exist
        Integer exist = 0;
        Integer i = 0;
        while (exist != 1 && i<userWishlist.size()){
            if (userWishlist.get(i).getIdWork() == book)
                exist = 1;
            else
                i++;
        }

        //save wishlist
        if (exist == 0){
            newWishlist.setIdWork(book);
            newWishlist.setIdUser(currentUserId);
            wishlistService.saveAndUpdate(newWishlist);
            return "redirect:/wishlists/mywishlist";
        } else{
            return "redirect:/wishlists/mywishlist";
        }
    }
}
