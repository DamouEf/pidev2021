package com.esprit.ouverage.repository;

import com.esprit.ouverage.model.User;
import com.esprit.ouverage.model.WishList;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface WishlistRepository extends JpaRepository<WishList, Long> {


    @Query("SELECT w FROM WishList w WHERE w.idUser = :user")
    List<WishList> findAllByIdUserOrOrderByCreatedDate(@Param("user") Long idUser);
}
