package com.esprit.ouverage.repository;

import com.esprit.ouverage.model.Feedback;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

public interface FeedbackRepository extends JpaRepository<Feedback,Long> {
    List<Feedback> getFeedbackByUserId(Long userId);
}
