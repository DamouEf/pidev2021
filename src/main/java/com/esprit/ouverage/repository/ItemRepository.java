package com.esprit.ouverage.repository;

import java.sql.Array;
import java.util.Date;
import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import com.esprit.ouverage.model.Item;
import com.esprit.ouverage.model.Operation;
import com.esprit.ouverage.model.User;
import com.esprit.ouverage.model.Work;
@Repository
public interface ItemRepository extends JpaRepository<Item,Long>{
	public List<Item> findByOperation(Operation operation);
	public List<Item> findByMember(User user);
//	@Query("SELECT p FROM Operation p" +
//            "WHERE p.OperationType  like :type" +
//			"AND p.idMembre = :user")
//	
//	 public List<Operation> findAllOperationByType(@Param("type") OperationType type,@Param("user") Long user);
	 
		//2-  les requettes specifiques aves des inconnus : les noms des requettes sont au choix 
	@Query(value="Select o from Item o where o.operation= :operation ")
	public List<Item> findItemsOperation(@Param("operation") Long operation) ;
	
	@Query(value="SELECT i.id,i.operation,i.work,i.expireDate,i.nbDays,i.startDate,i.quantity,i.total  FROM Item i,Operation p WHERE  i.operation = p.id "
			+ "AND p.idMembre = :user")	
			//+ "AND p.validate =0 And p.operationType =0")
	 public List<Item> findNonValidateItemCart(@Param("user") Long user);
	
	@Query(value="SELECT SUM(i.total) FROM Item i WHERE i.operation = :operation")
	 public Float SommeItemsCurentOperation(@Param("operation") Operation operation);

	@Query(value="SELECT COUNT(*) FROM Item i WHERE  i.operation = :operation and i.work=:work")
	 public long NBCurentItemWorkOperation(@Param("operation") Operation operation,@Param("work") Work work);		
	
	 @Query(value="SELECT i FROM Item i WHERE i.operation = :operation AND i.work = :work")
	 public Item findCurentItemWorkOperation(@Param("operation") Operation operation,@Param("work") Work work);
	 
	 @Query(value="SELECT i.id,i.nbDays,i.startDate,i.expireDate,i.quantity,i.total,i.returned "
	 		+ "FROM Item i WHERE  i.member = :user")
		 public List<Item> findNonReturnedBook(@Param("user") User user);
	 
	
}
