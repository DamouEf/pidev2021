package com.esprit.ouverage.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.esprit.ouverage.model.Offre;

@Repository
public interface OffreRepository extends JpaRepository<Offre, Long> {

}
