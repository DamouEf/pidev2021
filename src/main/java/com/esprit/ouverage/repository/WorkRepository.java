package com.esprit.ouverage.repository;


import java.util.List;
import java.util.ArrayList;
import java.util.Optional;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;
import com.esprit.ouverage.model.Work;

@Repository
	public interface WorkRepository extends JpaRepository<Work, Long> {
		public List<Work> getByTitle(String title);
		public List<Work> getByAuthor(String author);
		public List<Work> getByTitleAndAuthor(String title,String author);
		//public List<Work> workSearcher(String title, String author);
		
		@Query(value="Select w from Work w where w.title LIKE '%'||:title||'%' or w.author LIKE '%'||:author||'%'")
		public List<Work> workSearcher(@Param("title") String title,
				@Param("author") String author);
		
		@Query("select b from Work b  where b.title = :titleAuthor or b.author = :titleAuthor" )
		public ArrayList<Work> searchBooks(@Param("titleAuthor") String titleAuthor);

}
