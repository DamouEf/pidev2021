package com.esprit.ouverage.repository;

import com.esprit.ouverage.model.Claim;
import com.esprit.ouverage.model.StatusClaim;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.Date;
import java.util.List;


@Repository
public interface ClaimRepository extends JpaRepository<Claim,Long> {

    List<Claim> getClaimByUserId(Long userId);

    @Query("SELECT c FROM Claim c " +
            "WHERE c.userId.id = :user " +
            "and c.status = :status ")
    List<Claim> getClaimByStatusAndUserId(@Param("status") StatusClaim status,
                                          @Param("user") Long user);

    @Query("SELECT c FROM Claim c, User u " +
            "WHERE (u.firstName like %:pseudo% or u.lastName like %:pseudo%) " +
            "and u.id = c.userId.id " +
            "and c.status = :status")
    List<Claim> getClaimByStatusAndUserId(
            @Param("status") StatusClaim status,
            @Param("pseudo") String pseudo);

    List<Claim> getClaimByStatus(StatusClaim status);



}
