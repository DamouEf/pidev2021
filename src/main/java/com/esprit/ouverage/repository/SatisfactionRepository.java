package com.esprit.ouverage.repository;

import com.esprit.ouverage.model.Satisfaction;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface SatisfactionRepository extends JpaRepository<Satisfaction,Long>{
    List<Satisfaction> getSatisfactionByUserId(Long userId);
}
