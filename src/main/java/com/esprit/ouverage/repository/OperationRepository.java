package com.esprit.ouverage.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import com.esprit.ouverage.model.Operation;
import com.esprit.ouverage.model.OperationType;
import com.esprit.ouverage.model.Work;
@Repository
public interface OperationRepository extends JpaRepository<Operation,Long>{

//	@Query("SELECT p FROM Operation p" +
//            "WHERE p.OperationType  like :type" +
//			"AND p.idMembre = :user")
//	
//	 public List<Operation> findAllOperationByType(@Param("type") OperationType type,@Param("user") Long user);
	 public List<Operation> findByOperationTypeAndIdMembreAndValidate(OperationType type,Long userid,boolean operationstatus);
	 public List<Operation> findByIdMembre(Long idmembre);
	 @Query(value="SELECT p FROM Operation p "
			 +"WHERE p.operationType = 0"
			 +"AND p.validate = 0"
		+"AND p.idMembre = :user")
	 public Operation findCurentCartOperation(@Param("user") Long userid);
	
	 @Query(value="SELECT p FROM Operation p "
			 +"WHERE p.operationType = 1"
			 +"AND p.validate = 0"
		+"AND p.idMembre = :user")
	 public Operation findCurentRentOperation(@Param("user") Long userid);
	 
	 @Query(value="SELECT Count(*) FROM Operation p "
			 +"WHERE p.operationType = 0"
			 +"AND p.validate = 0"
		+"AND p.idMembre = :user")
	 public int NBCurentCartOperation(@Param("user") Long userid);
	  
	 @Query(value="SELECT Count(*) FROM Operation p "
			 +"WHERE p.operationType = 1"
			 +"AND p.validate = 0"
		+"AND p.idMembre = :user")
	 public int NBCurentRentOperation(@Param("user") Long userid);
	 
	
	 
	
}
