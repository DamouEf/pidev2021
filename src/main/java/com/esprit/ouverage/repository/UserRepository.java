package com.esprit.ouverage.repository;

import com.esprit.ouverage.model.Item;
import com.esprit.ouverage.model.Operation;
import com.esprit.ouverage.model.User;
import com.esprit.ouverage.model.Work;

import java.util.ArrayList;
import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

@Repository
public interface UserRepository extends JpaRepository<User, Long> {
	public List<User> findByFirstNameOrLastName(String FirstName,String LastName);
	
	@Query(value="Select u from User u where u.firstName LIKE '%'||:firstname||'%' or u.lastName LIKE '%'||:lastname||'%'")
	public List<User> userSearche(@Param("firstname") String firstname, @Param("lastname") String lastname);
	
}
