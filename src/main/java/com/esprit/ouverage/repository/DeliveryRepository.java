package com.esprit.ouverage.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import com.esprit.ouverage.model.Delivery;

@Repository
public interface DeliveryRepository extends JpaRepository<Delivery, Long>
{

//	@Query(value="SELECT d.idDelivery,d.dateDelivery,d.svc  FROM Delivery d,Operation p WHERE  d.ido = p.id AND p.idMembre = :user")	
//			
//	 public List<Delivery> getUserDeliveries(@Param("user") Long user);


}
