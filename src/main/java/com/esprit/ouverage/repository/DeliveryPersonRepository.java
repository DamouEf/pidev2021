package com.esprit.ouverage.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import com.esprit.ouverage.model.DeliveryPerson;

public interface DeliveryPersonRepository extends JpaRepository<DeliveryPerson, Long>{
	
	@Query("SELECT c FROM DeliveryPerson c " +
            "WHERE c.rating >= :rating " 
            )

	  
	  List<DeliveryPerson> findByRating(@Param("rating") Integer rating);
	 
	 }
