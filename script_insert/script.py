from __future__ import print_function
from os import name
from re import X
import mysql.connector
from datetime import date, datetime, timedelta
import random
import json

cnx = mysql.connector.connect(user='root', password='',
                              host='127.0.0.1',
                              database='ouverages')
cursor = cnx.cursor()

# Constants 
ADRESS: list = ["Tunis", "Al Munastir", "Al Marsa", "Sousah", "Zouila", "El Mourouj", "Bizerte", "Aryanah", "dar chaabane"]

LANGUAGES: list = ["French", "English"]
AUTHORS : list = ['Victor Hugo','Paul Gravett','George Orwell', 'Paulo Coelho','Sobhi hamawi', 'JK Rowling', 'Ibn Arabi', 'René Goscinny ', 'Zep','Najib Mahfoudh', 'Emile Zola', 'Charles Baudelaire', 'Taha Hussine']
PUBLISHER : list = ['HACHETTE', 'Independently','ROCHER','eyrolles edition','Dar Al Machreq','Dar Al Maaref','Bloomsbury Publishing','Al Bouraq','Livre de poche', 'GLÉNAT','Hetzel-Quantin']
BOOL: list = [1,0]
OFFRES : list = ['Fête des mères', 'Foire des livres','Rentrée scolaire','Liquidation','Soldes']
STATUS_CLAIM: list = ["PENDING", "UNTREATED", "TREATED"]
CLAIM_SUBJECT: list =  ["Login_Errors", "Section_Loading", "Inappropriate_Content", "Privacy_Issues", "Profile_Configurations", "Others"]
OPERATION_TYPE: list = [0, 1]
SERVICE_TYPE: list = ["RentService","SaleService"]
NAME_DELIVERY_PERSON: list = ["Farid", "Omar", "Wael", "Lotfi", "Ahmed", "Slim", "Saber", "Nabil", "Samer" , "Jawher"]
LAST_NAME_DELIVERY_PERSON: list = ["Ben Fraj", "Ghimaji", "Lazreg", "Mokhtari", "Badrouche", "Hajri", "Ben Ammar" , "Ben Ali", "Ghanouchi", "Saied" , "Taboubi", "Fodhha wahdek" ]

# liste table 
list_tables: list = ['user','work','offre','wishlist','claim','feedback','delivery_person','operations','items', 'satisfaction', 'delivery']


# insert user
# insert offres
# insert works
# insert wishlists
# insert claims
# insert feedbacks
# insert delivery_person
# insert operations
# insert items
# insert delivery


# password : user for every user
# $2a$10$m/fcaai6vbxDIDtCXos79OARCVMK0uDiopRKE6V3FN3yg3ToAW71e

# INSERT INTO `work` (`id`, `author`, `category`, `edition`, `language`, `publisher`,`rentable`, `title`, `offre_id`, `price`, `quantity`) VALUES (NULL, NULL, NULL, '2021-07-15 22:25:19.000000', NULL, NULL, NULL, NULL, NULL, '', '')

# querry 
insert_user: str = "INSERT INTO `user` (`id`, `address`, `enabled`, `first_name`, `last_name`, `password`, `role`, `username`) VALUES (%(id)s, %(adress)s, b'1', %(first_name)s, %(last_name)s, '$2a$10$m/fcaai6vbxDIDtCXos79OARCVMK0uDiopRKE6V3FN3yg3ToAW71e', %(role)s, %(username)s)"
inser_work: str = "INSERT INTO `work` (`id`, `author`, `category`, `edition`, `language`, `publisher`,`rentable`, `title`, `offre_id`, `price`, `quantity`) VALUES (%(id)s, %(author)s, %(category)s, '2021-07-15 22:25:19.000000',  %(language)s, %(publisher)s, %(rentable)s, %(title)s, %(offre_id)s, %(price)s, %(quantity)s)"
inser_offre: str = "INSERT INTO `offre` (`id`, `percentage`, `end_date`, `start_date`, `name`) VALUES (%(id)s, %(percentage)s, '2021-07-15 23:36:11', '2021-07-23 23:36:11', %(name)s)"
insert_wishlist: str = "INSERT INTO wishlist (id, id_user, id_work_id) VALUES (%(id)s, %(id_user)s, %(id_work_id)s)"
insert_claim: str = "INSERT INTO claim (id, content, claim_date, status, subject, user_id_id) VALUES (%(id)s, %(content)s, %(claim_date)s, %(status)s, %(subject)s, %(user_id_id)s)"
insert_feedback: str = "INSERT INTO `feedback` (`id`, `message`, `rating`, `user_id_id`, `work_id_id`) VALUES (%(id)s, %(message)s, %(rating)s, %(user_id_id)s, %(work_id_id)s)"
insert_delivery_persone: str ="INSERT INTO `delivery_person` (`id`, `name`, `phone`, `rating`, `surname`) VALUES (%(id)s, %(name)s, %(phone)s, %(rating)s, %(surname)s)"
insert_operation: str = "INSERT INTO `operations` (`id`, `id_membre`, `operation_date`, `operation_type`, `sub_total`, `tax`, `total`, `validate`) VALUES (%(id)s, %(id_membre)s, %(operation_date)s, %(operation_type)s, %(sub_total)s, %(tax)s, %(total)s, %(validate)s)"
insert_items: str ="INSERT INTO `items` (`id`, `discount`, `expire_date`, `nb_days`, `quantity`, `return_date`, `returned`, `start_date`, `total`, `member`, `operation`, `work`) VALUES (%(id)s, %(discount)s,  %(expire_date)s,  %(nb_days)s,  %(quantity)s,  %(return_date)s,  %(returned)s,  %(start_date)s,  %(total)s,  %(member)s,  %(operation)s,  %(work)s)"
insert_satisfaction: str ="INSERT INTO `satisfaction` (`id`, `message`, `rating`, `operation_id_id`, `user_id_id`) VALUES (%(id)s, %(message)s, %(rating)s, %(operation_id_id)s, %(user_id_id)s)"
insert_delivery: str = "INSERT INTO `delivery` (`id_delivery`, `delivery_date`, `svc`, `ido`, `idp`, `delivered`) VALUES (%(id)s, %(delivery_date)s, %(svc)s, %(ido)s, %(idp)s, %(delivered)s)"

# '2021-07-15 18:15:16'

if __name__ == "__main__":
    # initial data
    with open("books.json", 'r') as jsonfile:
        books:list = json.load(jsonfile)

    with open("books_feedback.json", 'r') as jsonfile:
        feedbacks:list = json.load(jsonfile)
    
    # list names delivery
    list_names: list = []
    while len(list_names) < 30:
        list_names: list = list(set([
            f"{random.choice(NAME_DELIVERY_PERSON)} {random.choice(LAST_NAME_DELIVERY_PERSON)}" for index in range(1, 60)
        ]))

    # truncate table

    for table in list_tables:
        querry: str = f"TRUNCATE TABLE {table}"
        cursor.execute(querry)


    # create admin
    payload = {
        'id': 100,
        'adress': random.choice(ADRESS),
        'first_name': f"root",
        'last_name': f"root",
        'username': f"root",
        'role': "ROLE_ADMIN"        
    }
    cursor.execute(insert_user, payload)
    # create ROLE_LIBRARIAN
    payload = {
        'id': 101,
        'adress': random.choice(ADRESS),
        'first_name': f"librarian",
        'last_name': f"librarian",
        'username': f"librarian",
        'role': "ROLE_LIBRARIAN"        
    }
    cursor.execute(insert_user, payload)

    # create users 
    print("create 100 users start !")
    for index in range(103, 211):
        payload = {
            'id': index,
            'adress': random.choice(ADRESS),
            'first_name': f"user_{index}",
            'last_name': f"user_{index}",
            'username': f"user_{index}",
            'role': "ROLE_USER" 
        }


        # Insert new employee
        cursor.execute(insert_user, payload)
    print("create 100 users finished!")

    # creat offers
    print(f"create {len(OFFRES)} offers start!")
    for index,offre in enumerate(OFFRES):
        # initial data
        payload: dict = {
            'id': index+100,
            'percentage': random.randint(2,15),
            'name': offre
        }
        cursor.execute(inser_offre, payload)
    print(f"create {len(OFFRES)} offers finished!")

    # create works 
    print("create books start !")
    for index,book in enumerate(books):
        
        # initial data 
        author = random.choice(book["authors"]) if book["authors"] else random.choice(AUTHORS) 
        category = random.choice(book["categories"]) if book["categories"] else random.choice(AUTHORS)
        title = book.get("title")

        payload = {
            'id': index+100,
            'author': author,
            'category': category,
            'language': random.choice(LANGUAGES),
            'publisher': random.choice(PUBLISHER),
            'rentable': random.choice(BOOL),
            'title': title,
            'offre_id': random.randint(100, 104),
            'price' : random.randint(10, 50),
            'quantity': random.randint(0, 200)
        } 
        cursor.execute(inser_work, payload)
    print(f"create {len(books)} books finished! !")

    # create wish list
    print(f"Create wish list Start")
    id_wishlist: int = 100
    for user_id in range(103, 211):
        random_number: int = random.randint(1,20)
        for index in range(0, random_number):
            # create line in wishlist
            payload = {
                'id': id_wishlist,
                'id_user': user_id,
                'id_work_id': random.randint(100, len(books))
            } 
            cursor.execute(insert_wishlist, payload)
            id_wishlist += 1
    print(f"create wishlist finished! !")
      
    # create claim
    print(f"create claim start !")
    id_claim: int = 100
    for user_id in range(103, 210):
        random_number: int = random.randint(1,10)
        for index in range(0, random_number):
            # create line in claim 
            payload = {
                'id': id_claim,
                'content': "generate random content !",
                'claim_date': f"2021-{random.randint(1,12)}-{random.randint(1,28)}",
                'status': random.choice(STATUS_CLAIM),
                'subject': random.choice(CLAIM_SUBJECT),
                'user_id_id': user_id
            }
            cursor.execute(insert_claim, payload)
            id_claim += 1
    print(f"create claim finished!")

    # create feedbacks
    print(f"Create feedback start !")
    id_feedback: int = 100
    for book_id in range(100 , len(books)):
        for index in range(0 , random.randint(5 , 50)):
            random_rating: str = random.choice(list(feedbacks.keys()))
            random_message: str = random.choice(feedbacks.get(random_rating))
            payload = {
                'id': id_feedback,
                'message': random_message,
                "rating": random_rating,
                "user_id_id": random.randint(103, 210),
                "work_id_id": book_id
            }
            cursor.execute(insert_feedback, payload)
            id_feedback += 1
    print("create feedback finished !")

    # create delivery person
    print(f"Create delivery person start!")
    index_person: int = 0
    for id_person in range(100 , 131):
        payload = {
            'id': id_person,
            'name': list_names[index_person],
            'phone': f"{random.randint(100, 999)}{random.randint(100, 999)}{id_person}",
            'rating': random.randint(1,5),
            'surname': f"Delivery person {id_person}"
        }
        cursor.execute(insert_delivery_persone, payload)
        index_person += 1
    print(f"create delivery persons finsished!")

    # create operations
    print(f"Create operation/items start !")
    id_item: int = 100
    id_satisfaction: int = 100
    id_delivery: int = 100
    for id_operation in range(100 , 1000):
        # initial data 
        total: int = random.randint(50 , 300)
        sub_total: int = 0.81*total
        tax: int = 0.19*total
        id_member: int = random.randint(103, 110)
        operation_date: str = f"2021-{random.randint(1,12)}-{random.randint(1,28)}"
        payload = {
            'id': id_operation,
            'id_membre': id_member,
            'operation_date': operation_date,
            'operation_type': random.choice(OPERATION_TYPE),
            'sub_total': sub_total,
            'tax': tax,
            'total': total,
            'validate': random.choice(BOOL)
        }
        cursor.execute(insert_operation, payload)
        # while total!= 0:
        for index in range(1, random.randint(1, 5)):
            # initial data
            total_item: int = random.randint(10, 50)
            total = total - total_item
            start_day: int = random.randint(1,10)
            month: int = random.randint(1,12)
            nb_days: int =  random.randint(3, 14)
            
            payload_item = {
                'id': id_item,
                'discount': random.randint(0 , 15),
                'expire_date': f"2021-{random.randint(1,12)}-{random.randint(1,28)}",
                'nb_days': nb_days,
                'quantity': random.randint(1, 3),
                'start_date': f"2021-{month}-{start_day}",
                'return_date': f"2021-{month}-{start_day+nb_days}",
                'returned': random.choice(BOOL),
                'total': total_item,
                'operation': id_operation,
                'work': random.randint(10, len(books)),
                'member': id_member
            }
            cursor.execute(insert_items, payload_item)
            id_item += 1
    
        # create satisfaction
        random_rating: str = random.choice(list(feedbacks.keys()))
        random_message: str = random.choice(feedbacks.get(random_rating))
        payload_satisfaction = {
            'id': id_satisfaction,
            'message': random_message,
            'rating': random_rating,
            'operation_id_id': id_operation,
            'user_id_id': id_member,
        }
        cursor.execute(insert_satisfaction, payload_satisfaction)
        id_satisfaction += 1

        # create delivery 
        payload_delivery =  {
            'id' : id_delivery,
            'delivery_date': operation_date,
            'svc': random.choice(SERVICE_TYPE),
            'ido': id_operation,
            'idp': random.randint(100 , 130),
            'delivered': random.choice(BOOL)
        }
        cursor.execute(insert_delivery, payload_delivery)
        id_delivery += 1
    print(f"Create operation/items/satisfactions/delivery finished !")
    
    # close connection with database 
    cnx.close()


